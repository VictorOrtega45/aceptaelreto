import java.util.Scanner;

public class Problema520 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, m, a, b;
		
		do
		{
			n = sc.nextInt();
			m = sc.nextInt();
			if(n == 0 && m == 0) break;
			
			do
			{
				a = sc.nextInt();
				b = sc.nextInt();
				if(a == 0 & b == 0) break;
				
				if(a == m) m = b;
				else if(b == m) m = a;			
			} while(true);
			
			System.out.println(m);
			
		} while(true);
	}
}
