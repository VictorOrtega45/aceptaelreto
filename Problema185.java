import java.util.Scanner;
import java.util.Arrays;
// @author V�ctor Ortega
public class Problema185 {
	static String[] palabraSi = new String[25];
	static String[] palabraNo = new String[25];
	static String[] salida = new String[25];
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	String entrada = "";
		int contadorSi = 0;	int contadorNo = 0;
		boolean repetit = false;
		int contadorSalida = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			n = sc.nextInt();
			if(n==0) break;
			
			contadorSi = 0;
			contadorNo = 0;
			contadorSalida = 0;
			
			for(int i = 0; i<n; i++) {
				entrada = sc.next();
				if(entrada.equals("SI:")) {
					do {
						palabraSi[contadorSi] = sc.next();
						if(palabraSi[contadorSi].equals("FIN")) break;
						contadorSi++;
					} while(true);
				}
				else {
					do {
						palabraNo[contadorNo] = sc.next();
						if(palabraNo[contadorNo].equals("FIN")) break;
						contadorNo++;
					} while(true);;
				}
			}
			
			for(int i = 0; i<contadorNo; i++) {
				repetit = false;
				for(int j = 0; j<contadorSi; j++) {
					if(palabraNo[i].equals(palabraSi[j])) {
						repetit = true;
						break;
					}
				}
				if(!repetit) {				
					salida[contadorSalida] = palabraNo[i];
					contadorSalida++;
				}
			}
			
			String sTemp = "";
			for(int i = 0; i<contadorSalida; i++) {
				for(int j = i+1; j<contadorSalida; j++) {
					if(salida[i].compareToIgnoreCase(salida[j])>0) {
						sTemp = salida[j];
						salida[j] = salida[i];
						salida[i] = sTemp;
					}
				}
			}
			
			for(int i = 0; i<contadorSalida; i++) {
				if(i==contadorSalida-1) System.out.print(salida[i]);
				else System.out.print(salida[i] + " ");
			}
			System.out.println();
			
		} while(true);
		sc.close();
	}
}
