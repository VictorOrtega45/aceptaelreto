import java.util.Scanner;

public class Problema168 
{
	public static void main(String[] ags)
	{
		Scanner sc = new Scanner(System.in);
		
		int 
			n,
			suma,
			sumaTotal;
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			suma = 0;
			sumaTotal = 0;
			
			for(int i = 1; i < n; i++)
			{
				suma += sc.nextInt();
				sumaTotal += i;
			}
			
			System.out.println((sumaTotal+n) - suma);
			
		} while(true);
	}
}
