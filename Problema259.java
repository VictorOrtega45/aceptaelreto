import java.util.Scanner;

public class Problema259 
{
	public static void main(String[] args)
	{
		int n = 0;
		int llargada = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do
		{
			n = sc.nextInt();
			
			if(n == 0) break;
			
			if(n == 1) 
			{
				System.out.println("10");
				continue;
			}
			
			if(n % 2 == 0) n--;
			
			n/=2;
			System.out.print("9");
			while(n>=1)
			{
				System.out.print("0");
				n--;
			}
			System.out.println();
			
		} while(true);
	}
}
