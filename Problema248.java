import java.util.Scanner;

public class Problema248 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		int precio, pagado, euro200, euro100, euro50, euro20, euro10, euro5, euro2, euro1;
		
		while(n > 0)
		{
			precio = sc.nextInt();
			pagado = sc.nextInt();
			
			euro200 = 0;
			euro100 = 0;
			euro50 = 0;
			euro20 = 0;
			euro10 = 0;
			euro5 = 0;
			euro2 = 0;
			euro1 = 0;
			
			if(pagado < precio)
			{
				System.out.println("DEBE " + (precio - pagado));
			}
			else
			{
				pagado -= precio;
				if(pagado / 200 >= 1) 
				{
					euro200 = pagado / 200;
					pagado %= 200;
				}
				if(pagado / 100 >= 1) 
				{
					euro100 = pagado / 100;
					pagado %= 100;
				}
				if(pagado / 50 >= 1) 
				{
					euro50 = pagado / 50;
					pagado %= 50;
				}
				if(pagado / 20 >= 1) 
				{
					euro20 = pagado / 20;
					pagado %= 20;
				}
				if(pagado / 10 >= 1) 
				{
					euro10 = pagado / 10;
					pagado %= 10;
				}
				if(pagado / 5 >= 1) 
				{
					euro5 = pagado / 5;
					pagado %= 5;
				}
				if(pagado / 2 >= 1) 
				{
					euro2 = pagado / 2;
					pagado %= 2;
				}
				if(pagado / 1 >= 1) 
				{
					euro1 = pagado / 1;
				}
				System.out.printf("%d %d %d %d %d %d %d %d%n",
						euro200,
						euro100,
						euro50,
						euro20,
						euro10,
						euro5,
						euro2,
						euro1);
			}
			--n;
		}
	}
}
