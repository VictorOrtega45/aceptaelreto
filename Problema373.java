import java.util.Scanner;
// @author VíctorOrtega
public class Problema373 {
	public static void main(String[] args) {
		int n = 0;	int dimensio = 0;	int calcul = 0;
		int dimensioB = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		while(n>0) {
			dimensio = sc.nextInt();
			
			dimensioB = dimensio-2;
			
			calcul = (int) Math.pow(dimensioB, 2)*dimensioB;
			
			calcul = (int) Math.pow(dimensio, 3) - calcul;
			
			System.out.println(calcul);
			n--;
		}
		sc.close();
	}
}
