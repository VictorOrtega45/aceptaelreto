import java.util.Scanner;
import java.util.Arrays;

public class Problema478 {
	public static void main(String[] args)
	{
		int n = 0;
		long da�o = 0;
		long da�oTotal = 0;
		int index = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			long[] hech = new long[n];
			
			da�oTotal = 0;
			
			for(int i = 0; i < n; i++)
			{
				hech[i] = sc.nextInt();
				da�oTotal+= hech[i];
			}
			
			da�o = da�oTotal - sc.nextLong();
			
			for(int i = 0; i < n; i++)
			{
				index = Arrays.binarySearch(hech, da�o-hech[i]);
				
				if(index >= 0)
				{
					System.out.println(hech[i] + " " + hech[index]);
					break;
				}
			}
			
		} while(true);
		
	}
}
