import java.util.Scanner;
// @author V�ctor Ortega
public class Problema192 {
	public static void main(String[] args) {
		// Declaraci� variables
		Scanner sc = new Scanner(System.in);
		int n = 0;	boolean correcte = false;
		
		do {
			n = sc.nextInt();
			if(n==0) break;
			correcte = false;
			
			do {
				if(n%3==1) {
					correcte=true;
					break;
				}
				if(n-5>=1) {
					n-=5;
					continue;
				}
				if(n<6)break;
			} while(n>1);
			
			if(correcte)System.out.println("SI");
			else System.out.println("NO");
			
		} while(true);
	}
}
