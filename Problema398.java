import java.util.Scanner;
import java.util.ArrayList;
// @author VictorOrtega
class Boton {
	int x;
	int y;
	boolean hayBoton;
	int vaRaton;
	Boton(int ejeX, int ejeY, boolean boton) {
		this.x = ejeX;
		this.y = ejeY;
		this.hayBoton = boton;
	}
}

public class Problema398 {
	
	public static void main(String[] args) {
		// Declaraci� variables
		int f = 0;	int c = 0;	int n = 0;
		Scanner sc = new Scanner(System.in);
		int botonX = 0;	int botonY = 0;	
		
		do {
			f = sc.nextInt();
			c = sc.nextInt();
			
			ArrayList<Boton> boton = new ArrayList<Boton>();
			// boolean[][] tauler = new boolean[f][c];
			int ratoli1X = 1;
			int ratoli1Y = 1;
			int ratoli2X = 1;
			int ratoli2Y = 1;
			int contadorBotones = 0;
			boolean acabat = false;
			
			n = sc.nextInt();
			
			// Agafem posicions botons
			for (int i = 0; i<n; i++) {
				botonX = sc.nextInt();
				botonY = sc.nextInt();
				
				boton.add(new Boton(botonX, botonY, true));
				contadorBotones++;
			}
			
			// Algorisme
			for (int i = 0; !acabat; i++) {
				
				int diferenciaRatoli1 = 0;
				int diferenciaRatoli2 = 0;
				
				diferenciaRatoli1 = Math.abs(ratoli1X-boton.get(i).x) + Math.abs(ratoli1Y-boton.get(i).y); 
				diferenciaRatoli2 = Math.abs(ratoli2X-boton.get(i).x) + Math.abs(ratoli2Y-boton.get(i).y);
				
				
				/* 
				 * vaRaton = 0 No va ning�n rat�n
				 * vaRaton = 1 Va el raton1
				 * vaRaton = 2 Va el raton2
				 */
				if (diferenciaRatoli1>diferenciaRatoli2) boton.get(i).vaRaton = 1;
				if (diferenciaRatoli2>diferenciaRatoli1) boton.get(i).vaRaton = 2;
				else boton.get(i).vaRaton = 1;
				
				
			} 
			
		} while (true);
	}
}
