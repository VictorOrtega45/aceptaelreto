import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
// @author V�ctor Ortega
public class Problema140 {
	public static void main(String[] args) {
		// Declaraci� variables
		String entrada = ""; int salida = 0;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(br);
		
		do {
			entrada = sc.next();
			if(Integer.parseInt(entrada)<0) break;
			salida = 0;
			
			char[] grup = entrada.toCharArray();
			
			for(char c : grup) {
				salida+= Character.getNumericValue(c);
			}
			
			for(int i = 0; i<grup.length-1; i++) {
				System.out.print(grup[i] + " + ");
			}
			System.out.println(grup[grup.length-1] + " = " + salida);
			
		} while(true);
		
		
	}
}
