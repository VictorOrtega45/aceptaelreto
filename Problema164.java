import java.util.Scanner;

public class Problema164 
{
	public static void main(String[] args)
	{
		int x1, y1, x2, y2;
		int area;
		
		Scanner sc = new Scanner(System.in);
		
		do
		{
			x1 = sc.nextInt();
			y1 = sc.nextInt();
			x2 = sc.nextInt();
			y2 = sc.nextInt();
			
			if(x2 < x1 || y2 < y1) break;
			
			area = (x2 - x1) * (y2 - y1);
			
			
			System.out.println(area);
		} while(true);
	}
}
