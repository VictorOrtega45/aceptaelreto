import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema157 {
	/*
	 * Dice los dias que quedan para acabar el a�o al entrar un dia y un mes
	 *  en la misma linea
	 */
	/*case 4:case 6:case 9:case 11: mes=30;
		break;
		case 2: mes=28;
		break;
		case 1:case 3:case 5:case 7:case 8:case 10:case 12: mes=31;
		*/
	 static int getMes(int mes) {
		switch (mes) {
		case 1: mes=0; break;
		case 2: mes=31; break;
		case 3: mes=59; break;
		case 4: mes=90; break;
		case 5: mes=120; break;
		case 6: mes=151; break;
		case 7: mes=181; break;
		case 8: mes=212; break;
		case 9: mes=243; break;
		case 10: mes=273; break;
		case 11: mes=304; break;
		case 12: mes=334;
		}
		return mes;
	}
	public static void main(String[] args) {
		
		// Declaraci� variables
		String entrada = "";
		int dia = 0;
		int mes = 0;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// Algorisme
		try{
			for (int i=Integer.parseInt(br.readLine());i>0;i+=(~1>>2)) {
				entrada=br.readLine();
				String[] partir=entrada.split(" ");
				dia = Integer.parseInt(partir[0]);
				mes = Integer.parseInt(partir[1]);
				mes=getMes(mes);
				System.out.println((365-(dia+mes)));
			}
		} catch(java.io.IOException e) {}
	}
}
