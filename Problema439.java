import java.util.Scanner;
// @author V�ctor Ortega
public class Problema439 {
	
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	
		String entrada = ""; 
		int salida = 0; 
		int d = 0;	int v = 0;	int t = 0;
		boolean dExist = false;	boolean vExist = false; boolean tExist = false; 
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		
		for (int i = n; i>0; i--) {
			dExist = false;
			vExist = false;
			tExist = false;
			
			for (int j = 0; j<2; j++) {
				entrada = sc.next();
				
				switch (entrada.charAt(0)){
					case 'D': dExist = true; 
					d=Integer.valueOf(entrada.substring(2)); 
					break;
					case 'V': vExist = true;
					v=Integer.valueOf(entrada.substring(2)); 
					break;
					case 'T': tExist = true; t=Integer.valueOf(entrada.substring(2)); 
				}
			}

			
			if(!dExist) System.out.println("D="+v*t);
			else if(!vExist) System.out.println("V="+d/t);
			else System.out.println("T="+d/v);
					
			
		}
	}
}
