import java.util.Scanner;
public class Problema514 {
	final static int pEntrada=0;
	final static int pValor=1;
	private static int[][] atur;
	private static int calcularMesosAturSup(int pos, int entrada) 
	{
		int n=0,salt=0;
		if(atur[pos-1][pEntrada]>entrada) 
		{
			if(pos>0) 
			{
				pos--;
				while (atur[pos][pEntrada]>entrada) 
				{
					salt=atur[pos][pValor];
					if (salt>0) 
					{
						n+=salt;
						pos=pos-salt;
					}
					else 
					{
						n++;
						pos--;
					}
					if (pos<0) return(n);
				}
				return(n);
			}		
			return(n);
		}
		else 
		{
			atur[pos][pValor]=0;
			return(0);
		}	
	}

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int n,entrada,nAturSup;
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			atur=new int[n][2];
			entrada = sc.nextInt();
			atur[0][pEntrada]=entrada;
			atur[0][pValor]=0;	
			for(int pos = 1; pos < n; pos++) 
			{
				entrada = sc.nextInt();
				atur[pos][pEntrada]=entrada;
				nAturSup=calcularMesosAturSup(pos, entrada);
				atur[pos][pValor]=nAturSup;
			}
			for (int i=0;i<n-1;i++) 
			{
				System.out.print(atur[i][pValor]+" ");
			}
			System.out.println(atur[n-1][pValor]);

		} while(true);
		sc.close();
	}
}

