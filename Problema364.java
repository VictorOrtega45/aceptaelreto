// @author VíctorOrtega
public class Problema364 {
		public static void main(String[] args) throws Exception {
		// Declaració variables
		int n = 0;
		String entrada = "";	
		StringBuilder sb = new StringBuilder();
		final char salt = '\n';
		
		do {
			entrada = "";
			while((n = System.in.read())!=10) {
				if(n==90) entrada+=(char)65; // z
				else if(n==32||n==13) entrada+=(char) n; // espacio
				else entrada+=(char) (n+1);
			} 
			
			if(entrada.length()==3&&entrada.substring(0,3).equals("GJO")) break;
			sb.append(entrada).append(salt);
			
		} while(true);
		System.out.print(sb);
	}
}