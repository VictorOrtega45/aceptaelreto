import java.util.Scanner;
// @author V�ctor Ortega
public class Problema380 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0; int entrada = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			n = sc.nextInt();
			if (n==0) break;
			entrada = 0;
			for(int i = n; i>0; i--) {
				entrada+=sc.nextInt();
			}
			System.out.println(entrada);
		} while(true);
	}
}
