import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema366 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int numero = 0;	String entrada = "";
		int dia = 0;	int mes = 0;
		
		try {numero = Integer.parseInt(br.readLine());}
		catch (Exception e) {}
		
		for(int i = numero;i>0; i--) {
			try { entrada = br.readLine();}
			catch (Exception e) {}
			
			String[] parts = entrada.split(" ");
			dia = Integer.valueOf(parts[0]);
			mes = Integer.valueOf(parts[1]);
			
			if(dia==25&&mes==12) System.out.println("SI");
			else System.out.println("NO");
		}
		
	}
}
