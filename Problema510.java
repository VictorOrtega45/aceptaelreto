import java.util.Scanner;
import java.util.Stack;
import java.util.Collections;

public class Problema510 
{
	public static void main(String[] args)
	{
		int n;
		int input;
		Stack<Integer> negativo = new Stack<Integer>();	
		Stack<Integer> positivo = new Stack<Integer>();
		int[] todo = new int[1000];
		
		Scanner sc = new Scanner(System.in);
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			for(int i = 0; i<n; i++)
			{
				input = sc.nextInt();
				todo[i] = input;
				if(input<0) negativo.add(input);
				else positivo.add(input);
			}
			
			Collections.sort(positivo);
			Collections.sort(negativo);
			Collections.reverse(positivo);
			
			for(int i = 0; i<n-1; i++)
			{
				if(todo[i]<0) System.out.print(negativo.pop()+" ");
				else System.out.print(positivo.pop()+" ");
			}
			if(todo[n-1]<0) System.out.print(negativo.pop());
			else System.out.print(positivo.pop());
			
			System.out.println();
		} while(true);
	}
}