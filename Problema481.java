import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema481 {
	public static void main(String[] args) throws Exception {
		// Declaraci� variables
		int a = 0;	int b = 0;	String entrada = ""; char s = '\n';
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			entrada = br.readLine();
			
			String[] parts = entrada.split(" ");
			
			a = Integer.parseInt(parts[0]);
			b = Integer.parseInt(parts[1]);
			
			if(a==0) break;
			
			switch(a) {
			case 1:sb.append('h').append(b).append(s); break;
			case 2:sb.append('g').append(b).append(s); break;
			case 3:sb.append('f').append(b).append(s); break;
			case 4:sb.append('e').append(b).append(s); break;
			case 5:sb.append('d').append(b).append(s); break;
			case 6:sb.append('c').append(b).append(s); break;
			case 7:sb.append('b').append(b).append(s); break;
			case 8:sb.append('a').append(b).append(s);
			}
			
		} while(true);
		System.out.print(sb.toString());
	}
}
