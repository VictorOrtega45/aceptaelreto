import java.util.Scanner;
// @author V�ctor Ortega
public class Problema233 {
	public static void main(String[] args) {
		// Declaraci� variables
		Scanner sc = new Scanner(System.in);
		int entrada = 0; int resta = 0;
		String sortida = "";
		
		do {
			entrada = sc.nextInt();
			if(entrada==0)	break;
			sortida = "";
			resta = 9;
			
			do {
				if(entrada-resta>=0) {
					sortida=String.valueOf(resta)+sortida;
					entrada-=resta;
				}
				else resta--;
				if(entrada==0) break;
			} while(true);
			
			System.out.println(sortida);
		} while(true);
		
	}
}
