import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Problema503 
{
	public static void main(String[] args) throws Exception
	{
		// Declaració variables
		int n = 0;	int dado1 = 0;	int dado2 = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int resta;
		int i;
		String entrada;
		StringBuilder sb = new StringBuilder();
		char espai = ' ';	char salt = '\n';
		
		n = Integer.parseInt(br.readLine());
		
		while(n>0) 
		{
			entrada = br.readLine();
			String[] parts = entrada.split(" ");
			dado1 = Integer.parseInt(parts[0]);
			dado2 = Integer.parseInt(parts[1]);
			
			
			resta = Math.abs(dado1-dado2);
			if(dado1>dado2) i = dado2;
			else i = dado1;
			
			i++;
			for(int j = 0; j<resta; j++)
			{
				sb.append(i).append(espai);
				i++;
			}
			sb.append(i).append(salt);
			
			n--;
		}
		System.out.print(sb);
	}
}