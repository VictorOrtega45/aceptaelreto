import java.util.Scanner;

public class Problema219 
{
	public static void main(String[] args)
	{
		int n = 0;
		int cantidad;
		int resultado;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0)
		{
			cantidad = sc.nextInt();
			resultado = 0;
			
			while(cantidad>0)
			{
				if (sc.nextInt()%2==0) resultado++;
				cantidad--;
			}
			
			System.out.println(resultado);
			
			n--;
		}
	}
}
