import java.util.Scanner;

public class Problema515 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n;
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			if(n == 1) System.out.println("1");
			else if(n % 2 == 0) System.out.println(n);
			else System.out.println(n - 1);
		} while(true);
	}
}
