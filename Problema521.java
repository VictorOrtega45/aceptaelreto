import java.util.HashMap;
import java.util.Scanner;

public class Problema521 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int p, l, a;
		double asistentes;
		
		HashMap<String, Boolean> vecinos;
		
		do
		{
			p = sc.nextInt();
			l = sc.nextInt();
			a = sc.nextInt();
			
			if(p == 0) break;
			
			vecinos = new HashMap<>();
			asistentes = 0;
			
			for(int i = 0; i < a; i++)
			{
				int cantidad = sc.nextInt();
				String s = sc.next();
				
				s += cantidad;
				
				if(!vecinos.containsKey(s))
				{
					vecinos.put(s, true);	
					++asistentes;
				}
			}
			
			if(asistentes >= Math.ceil((double) p * l / 2)) System.out.println("EMPEZAMOS");
			else System.out.println("ESPERAMOS");
			
		} while(true);
			
	}
}
