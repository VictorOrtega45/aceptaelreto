import java.util.Arrays;
import java.util.Scanner;
// @author VíctorOrtega
public class Problema356 
{
	public static void main(String[] args) 
	{
		int n = 0;
		long[] precios = new long[3];
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while (n>0) 
		{
			precios[0] = sc.nextLong();
			precios[1] = sc.nextLong();
			precios[2] = sc.nextLong();
			
			Arrays.sort(precios);
			
			System.out.print(precios[0] + " ");
			System.out.print(precios[1] + " ");
			System.out.println(precios[2]);
			
			n--;
		}
	}
}
