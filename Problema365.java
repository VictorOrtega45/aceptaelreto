import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;
// @author VíctorOrtega

public class Problema365 {
	
	static class Nino {
		boolean aitor = false;
		int regalos = 0;
		Nino(int r){
			this.regalos = r;
		}
		Nino(int r, boolean a){
			this.regalos = r;
			this.aitor = a;
		}
	}	

	public static void main(String[] args) {
		// Declaració variables
		int n = 0;
		int numeroNinos = 0;	int posicion = 0;
		int tiempo = 0;
		
		Queue<Nino> q = new LinkedList<>();
	
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0) {
			numeroNinos = sc.nextInt();
			posicion = sc.nextInt();
			
			tiempo = 0;
			for(int i = 0; i<numeroNinos; i++) {
				if(i==posicion-1) q.add(new Nino(sc.nextInt(),1==1));
				else q.add(new Nino(sc.nextInt()));
				 
			}
			
			while(true) {
				Nino ni = q.poll();
				ni.regalos--;
				if(ni.regalos>0) q.add(ni);
				tiempo = tiempo+2;
				if(ni.aitor&&ni.regalos==0) break;
			}
			
			q.clear();
			System.out.println(tiempo);
			n--;
		}
	}
}