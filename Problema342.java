import java.util.Scanner;
// @author VíctorOrtega
public class Problema342 
{
	public static void main(String[] args)
	{
		int ini;	int fin;	int n;
		int k;	int resultado = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			ini = sc.nextInt();
			fin = sc.nextInt();
			n = sc.nextInt();
			resultado = 0;
			
			if(ini==0) break;
			
			k = sc.nextInt();
			int[] hipotesis = new int[k];
			
			for(int i = 0; i<k; i++)
			{
				hipotesis[i] = sc.nextInt();
				resultado = Math.abs(resultado-hipotesis[i]);
			}
			
			if (resultado==1) System.out.println("LO SABE");
			else System.out.println("NO LO SABE");
			
		} while(true);
		
	}
}
