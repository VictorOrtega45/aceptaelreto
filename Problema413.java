import java.util.Scanner;
// @author V�ctor Ortega
public class Problema413 {
	public static void main(String[] args) {
		// Declaraci� variables
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();	int x = 0;	int y = 0;	
		
		while(n>0) {
			x = sc.nextInt();
			y = sc.nextInt();
			
			int calcul = (x*y)/2;
			
			if((x*y)%2==0) System.out.println(calcul + " " + calcul);
			else System.out.println((calcul+1) + " " + calcul);
			
			n--;
		}
	}
}
