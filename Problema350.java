import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author VíctorOrtega
public class Problema350 {
	public static void main(String[] args) throws Exception {
		// Declaració variables
		double a = 0;	double b = 0;	
		double c = 0;	double s = 0;
		double area = 0;	String entrada = "";
		final char salt = '\n';
		
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			entrada = br.readLine();
			String[] parts = entrada.split(" ");
			a = Double.parseDouble(parts[0]);
			b = Double.parseDouble(parts[1]);
			if(a==0) break;
			
			c = Math.hypot(a, b);
			
			s = (a+b+c)/2;
			
			area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
			
			sb.append(String.format("%.1f", area)).append(salt);
			
		} while(true);
		System.out.print(sb);
	}
}