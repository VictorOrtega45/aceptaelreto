import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema105 {
	// Declaraci� variables
	static final char[][] a = new char[][] { new char[] {
				'M','A','R','T','E','S',' '},
		new char[] {
				'M','I','E','R','C','O','L','E','S',' '
		},
		new char[] {
				'J','U','E','V','E','S',' '
		},
		new char[] {
				'V','I','E','R','N','E','S', ' '
		},
		new char[] {
				'S','A','B','A','D','O',' '
		},
		new char[] {
				'D','O','M','I','N','G','O', ' '
		}
	};
	static double entrada = 0d;	static double max = 0d;	static double min = 0d;
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static double media = 0;
	static StringBuilder sb = new StringBuilder(5000);
	static final char salt = '\n';
	static int maxI = 0;	static int minI = 0;
	
	public static void main(String[] args) throws Exception {
			
		do {
			entrada = Double.parseDouble(br.readLine());
			if (entrada==-1) break;
			
			minI = 5;
			maxI = 5;
			max = entrada;
			min = entrada;
			media = entrada;
			
			entrada = Double.parseDouble(br.readLine());
			media+=entrada;
				
			if (entrada>max) {
				max = entrada; 
				maxI = 0;
			}
			if (entrada<min) {
				min = entrada;
				minI = 0;
			}
			entrada = Double.parseDouble(br.readLine());
			media+=entrada;
			
			if (entrada>max) {
				max = entrada; 
				maxI = 1;
			}
			if (entrada<min) {
				min = entrada;
				minI = 1;
			}
			entrada = Double.parseDouble(br.readLine());
			media+=entrada;
				
			if (entrada>max) {
				max = entrada; 
				maxI = 2;
			}
			if (entrada<min) {
				min = entrada;
				minI = 2;
			}
			entrada = Double.parseDouble(br.readLine());
			media+=entrada;
				
			if (entrada>max) {
				max = entrada; 
				maxI = 3;
			}
			if (entrada<min) {
				min = entrada;
				minI = 3;
			}
			entrada = Double.parseDouble(br.readLine());
			media+=entrada;
				
			if (entrada>max) {
				max = entrada; 
				maxI = 4;
			}
			if (entrada<min) {
				min = entrada;
				minI = 4;
			}
			
			media*=0.166;
			
				switch(maxI) {
					case 5: sb.append(a[0]); break;
					case 0: sb.append(a[1]); break;
					case 1: sb.append(a[2]); break;
					case 2: sb.append(a[3]); break;
					case 3: sb.append(a[4]); break;
					case 4: sb.append(a[5]); 
				}
				switch(minI) {
				case 5: sb.append(a[0]); break;
				case 0: sb.append(a[1]); break;
				case 1: sb.append(a[2]); break;
				case 2: sb.append(a[3]); break;
				case 3: sb.append(a[4]); break;
				case 4: sb.append(a[5]); 
				}
			
			if (entrada>media) sb.append("SI").append(salt);
			else sb.append("NO").append(salt);
			
		} while(true);
		System.out.print(sb);
	}
}