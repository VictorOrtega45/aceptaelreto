import java.util.Scanner;
// @author VíctorOrtega
public class Problema247 {
	public static void main(String[] args) {
		int n = 0;	int numero = 0;
		int hNumero = 0;	boolean respuesta = false;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			n = sc.nextInt();
			if(n==0) break;
			
			respuesta = true;
			
			
			for(int i = 0; i<n; i++) {
				numero = sc.nextInt();
				
				if(i>0&&numero<=hNumero) {
					respuesta = false;
				}
				hNumero = numero;
			}
			
			if(respuesta) System.out.println("SI");
			else System.out.println("NO");
			
			n--;
		} while(true);
	}
}
