import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema155 {
	public static void main(String[] args) {
		int amplada = 0;	int calcul = 0;
		int altura = 0;
		String entrada = "";
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
		try{entrada = br.readLine();}
		catch (Exception e) {}
		
		String[] split = entrada.split(" ");
		amplada = Integer.parseInt(split[0]);
		if (amplada<0) break;
		altura = Integer.parseInt(split[1]);
		if (altura<0) break;
		
		calcul = (amplada+altura)*2;
		
		System.out.println(calcul);
		
		} while(true);
	}
}
