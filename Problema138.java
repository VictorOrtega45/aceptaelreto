import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema138 
{
	public static void main(String[] args) throws Exception
	{
		int n = 0;
		int entrada = 0;
		int resultado;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		
		n = Integer.parseInt(br.readLine());
		
		while(n>0)
		{
			entrada = Integer.parseInt(br.readLine());
			resultado = 0;		
	
			while(entrada>0) resultado+= entrada/=5;
	
			sb.append(resultado);
			n--;
		}
		System.out.print(sb);
	}
}
