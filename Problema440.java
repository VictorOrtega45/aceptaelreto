import java.util.Scanner;

public class Problema440 {

	public static void main(String[] args) {
		int n;
		int[] personas;
		int min, 
			p, 
			max,
			total;
		Scanner sc = new Scanner(System.in);
		
		do {
			
			n = sc.nextInt();
			if(n == 0) break;
			personas = new int[n];
			personas[0] = sc.nextInt();
			p = 1;
			total = 1;
			max = 1;
			min = 999999;
			for(int i = 1; i < n; i++)
			{
				personas[i] = sc.nextInt();
				if(personas[i] >= personas[i-1])
				{
					++p;
				}
				else 
				{
					if(p < min) min = p;
					if(p > max) max = p;
					++total;
					p = 1;
				}	
			}
			if(p < min) min = p;
			
			
			System.out.println(total + " " + min + " " + max);
		} while(true);
		
	}
	
}