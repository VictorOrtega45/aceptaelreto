import java.util.Scanner;

public class Problema293 
{
	public static void main(String[] args)
	{
		int n = 0;
		int resultado;
		int escolo = 0;
		int anillos = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0)
		{
			resultado = 0;
			
			resultado+=sc.nextInt()*6;
			resultado+=sc.nextInt()*8;
			resultado+=sc.nextInt()*10;
			escolo = sc.nextInt();
			anillos = sc.nextInt()*2;
			
			resultado+=escolo*anillos;
			
			System.out.println(resultado);
			
			n--;	
		}
	}
}
