import java.util.HashMap;
import java.util.Scanner;

public class Problema109 
{
	public static void main(String[] args) 
	{
		String inutil = "";
		String equipo1, equipo2;
		int sets1, sets2;
		int contador = 0;
		int max = 0;
		boolean empate;
		String salida = "";
		
		HashMap<String, Integer> parejas = new HashMap<String, Integer>();
		Scanner sc = new Scanner(System.in);
		
		
		do {
			inutil = sc.next();
			if(inutil.equals("FIN")) break;
			max = 0;
			empate = false;
			contador = 0;
			
			do {
				equipo1 = sc.next();
				if(equipo1.equals("FIN")) break;
				sets1 = sc.nextInt();
				equipo2 = sc.next();
				sets2 = sc.nextInt();
				
				if(parejas.get(equipo1)==null) parejas.put(equipo1, 0);
				if(parejas.get(equipo2)==null) parejas.put(equipo2, 0);
				
				
				if(sets1>sets2)
				{
					parejas.put(equipo1, parejas.get(equipo1)+2);
					parejas.put(equipo2, parejas.get(equipo2)+1);
				}
				else 
				{
					parejas.put(equipo2, parejas.get(equipo2)+2);
					parejas.put(equipo1, parejas.get(equipo1)+1);
				}
				
				contador++;
			} while(true);

			for(String s : parejas.keySet())
			{
				if(parejas.get(s)>max) 
					{
						salida = s;
						max = parejas.get(s);
					}
				else if(parejas.get(s)==max) 
				{
					empate = true;
					break;
				}
			}
			
			if(empate) System.out.println("EMPATE " + (parejas.size()*parejas.size()-parejas.size()-contador));
			else System.out.println(salida + " " + (parejas.size()*parejas.size()-parejas.size()-contador));
			
			parejas.clear();
		} while(true);
	}
}
