import java.util.Scanner;

public class Problema525 
{    
    public static void main(String[] args)
    {
	Scanner sc = new Scanner(System.in);
	
	int 
		n = sc.nextInt(),
		a,
		b,
		c,
		d,
		max;
	
	while(n > 0)
	{
	    a = sc.nextInt();
	    b = sc.nextInt();
	    c = sc.nextInt();
	    d = sc.nextInt();
	    
	    max = Math.max(a, b);
	    if(max == a)
	    {
		a = b;
		b = a;
	    }
	    
	    max = Math.max(c, d);
	    if(max == c)
	    {
		c = d;
		d = c;
	    }
	    
	    
	    if(b > d)
	    {
		int temp = c;
		c = a;
		a = temp;
		temp = d;
		d = b;
		b = temp;
	    }
	    
	    if(a > c && a < d || b > c && b < d || (a == c && b == d || a != b && c != d))
	    {
		System.out.println("SOLAPADOS");
	    }
	    else
	    {
		System.out.println("SEPARADOS");
	    }
	    
	    --n;
	}
	
    }
}
