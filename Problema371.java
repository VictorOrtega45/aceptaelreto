import java.util.Scanner;
// @author V�ctor Ortega
public class Problema371 {
	public static void main(String[] args) {
		// Declaraci� variables
		int entrada = 0;
		Scanner sc = new Scanner(System.in);
		int salida = 0;
		
		do {
			entrada = sc.nextInt();
			if(entrada==0) break;
			salida = 0;
			
			while(entrada>0) {
				salida+=entrada;
				entrada--;
			}
			
			System.out.println(salida*3);
		} while(true);
	}
}
