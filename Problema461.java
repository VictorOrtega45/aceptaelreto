import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Base
{
	int tropasN, quedan;
	
	public Base(int tropasN, int quedan)
	{
		this.tropasN = tropasN;
		this.quedan = quedan;
	}
	
}
public class Problema461 
{		
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int n = 0,
				s = 0,
				b = 0,
				r = 0,
				auxi = 0,
				res = 0;
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			auxi = 0;
			res = 0;
			ArrayList<Base> pq = new ArrayList<>();
			
			for(int i=0; i<n; i++)
			{
				s = sc.nextInt();
				b = sc.nextInt();
				r = sc.nextInt();
				
				s = Math.max(s, b+r);
				if(s <= (b+r)) b = 0;
				else b = s - b - r;
				pq.add(new Base(s, b));
			}
			Collections.sort(pq, new Comparator<Base>()
			{
				public int compare(Base b1, Base b2)
				{
					if (b1.quedan != b2.quedan)
						return -Integer.compare(b1.quedan, b2.quedan);
					else 
						return Integer.compare(b1.tropasN, b2.tropasN);
				}
			});
			
			while(!pq.isEmpty())
			{
				Base aux = pq.get(0);
				pq.remove(0);
				if(auxi < aux.tropasN) 
				{
					res += (aux.tropasN-auxi);
					auxi = aux.quedan;
				}
				else
				{
					auxi -= aux.tropasN;
					auxi += aux.quedan;
				}
			}
			System.out.println(res);
			
		} while(true);
	}
}
