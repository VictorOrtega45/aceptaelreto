import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema455 
{
	public static void main(String[] args) throws Exception
	{
		int n;
		int numero;
		int numTemp;
		final int DIEZMIL = 10000;
		final char salt = '\n';
		int b;
		
		
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		n = Integer.parseInt(br.readLine());
		
		while(n>0)
		{
			numero = Integer.parseInt(br.readLine());
			
			b = DIEZMIL;
			while (b>0)
			{
				numTemp = b;
				b = numero % b;
				numero = numTemp;
			}
			
			sb.append(DIEZMIL/numero).append(salt);
			
			n--;
		}
		System.out.print(sb);
	}
}
