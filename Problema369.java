import java.util.Scanner;
// @author VíctorOrtega
public class Problema369 {
	public static void main(String[] args) {
		// Declaració variables
		int n = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			n = sc.nextInt();
			if(n==0) break;
			
			while(n>0) {
				System.out.print("1");
				n--;
			}
			System.out.println();
			
		} while(true);
	}
}
