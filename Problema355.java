import java.util.Scanner;
// @author V�ctor Ortega
public class Problema355 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	int entrada = 0; boolean bisiesto = false;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		for(int i = n; i>0; i--) {
			entrada = sc.nextInt();
			bisiesto = false;
			
			if(entrada%4==0) {
				if(entrada%100!=0) bisiesto = true;
				else if(entrada%400==0) bisiesto = true;
			}
			if(bisiesto) System.out.println("29");
			else System.out.println("28");
			
		}
	}
}
