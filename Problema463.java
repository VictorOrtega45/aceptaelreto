import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema463 
{
	public static void main(String[] args) throws Exception
	{
		String entrada;
		int contadorS;
		int contadorB;
		int altura = 0;
		int maxAltura = 0;
		int llargada;
		char[] c = new char[100];
		int[] posicion = new int[100];
		final char asterisc = '#';
		final char salt = '\n';
		final char barra = '/';
		final char contrabarra = '\\';
		final char espai = ' ';
		final char baixa = '_';
		
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		while((entrada = br.readLine())!=null)
		{
			contadorS = 0;
			contadorB = 0;
			c = entrada.toCharArray();
			maxAltura = 0;
			
			for(int i = 0; i<entrada.length(); i++)
			{
				try {
					switch(c[i])
					{
						case 'S': contadorS++;
						altura = -1;
						break;
						case 'B': contadorB++;
						default: altura = 0;
					}
				}
				catch (Exception e) {}
				
				altura+= Math.abs(contadorS-contadorB)+1;
				posicion[i] = altura;
				if(altura>maxAltura) maxAltura = altura;
			}
			maxAltura+=1;
			llargada = entrada.length()+1;
			
			for(int i = maxAltura; i>=0; i--)
			{
				for(int j = 0; j<=llargada; j++)
				{
					if(i==0||i==maxAltura||j==0||j==llargada) System.out.print(asterisc);
					else if(posicion[j-1]==i) {
						switch(c[j-1])
						{
						case 'I': System.out.print(baixa);
						break;
						case 'S': System.out.print(barra);
						break;
						case 'B': System.out.print(contrabarra);
						break;
						}
					}
					else System.out.print(espai);
				}
				System.out.print(salt);
			}
		}
	}
}
