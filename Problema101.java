import java.util.Scanner;
// @author V�ctorOrtega
public class Problema101 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	int index = 0;	int vertical = 0;
		int horizontal = 0;	int diagonal = 0;	int lateral = 0;
		int limite = 0;	int salida = 0;	int cm2 = 0;	int cm = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			n = sc.nextInt();
			if(n==0) break;
			
			int[][] casilla = new int[n][n];
			index = 0;	
			vertical = 0;	
			horizontal = 0;	
			diagonal = 0;
			lateral = 0;
			limite = n-1;
			salida = 0;
			
			for(int i = 0; i<n; i++) {
				for(int j = 0; j<n; j++) {
					casilla[i][j] = sc.nextInt();
					if(j==index) diagonal+=casilla[i][j];
				}
				if(i==0) {
					lateral+=casilla[0][limite]; //limite = n-1
					lateral+=casilla[0][0];
					for(int k = 0; k<n; k++) {
						horizontal+=casilla[0][k];
					}
				}
				if(i==limite) {
					lateral+=casilla[i][0];
					lateral+=casilla[i][limite]; //limite = n-1
				}
				vertical+=casilla[i][0];
				index++;
			}
			
			if(horizontal==vertical&&vertical==diagonal) {
				cm = vertical;
				cm2 = 4*cm/n;
				salida = 1; // diabolico 
			}
			
			if(salida==1) {
				if(cm2==lateral) { // n�2	
					boolean escalera = false;
					int contador = 1;
					
					while(contador<=n*n) {
						escalera=false;
						for(int i = 0; i<n; i++) { 
							for(int j = 0; j<n; j++) {
								if(contador==casilla[i][j]) {
									escalera = true;
									break;
								}
							}
							if(escalera) break;
						}
						if(!escalera) break;
						contador++;
					}
					
					if(escalera) { // n�1	
						if(n%2==0) { // n�4
							int pivot = (int) Math.floor(limite/2);
							int pivot2 = pivot+1;
							int lateral2 = 0;
							int central = 0;
							
							lateral2+=casilla[0][pivot];
							lateral2+=casilla[0][pivot2];
							lateral2+=casilla[limite][pivot];
							lateral2+=casilla[limite][pivot2];
							lateral2+=casilla[pivot][0];
							lateral2+=casilla[pivot2][0];
							lateral2+=casilla[pivot][limite];
							lateral2+=casilla[pivot2][limite];
							
							if(lateral2==cm2*2) {
								central+=casilla[pivot][pivot];
								central+=casilla[pivot][pivot2];
								central+=casilla[pivot2][pivot];
								central+=casilla[pivot2][pivot2];
								
								if(central==cm2) salida=2;
							}
						}
						else { // n�3
							int pivot = (int) Math.floor(limite/2);
							int lateral2 = 0;
							
							lateral2+=casilla[0][pivot];
							lateral2+=casilla[limite][pivot];
							lateral2+=casilla[pivot][0];
							lateral2+=casilla[pivot][limite];
							
							if(lateral2==cm2) {
								if(casilla[pivot][pivot]*4==cm2) salida = 2;
							}
						}
					}
				}
			}
			
			switch(salida) {
				case 0: System.out.println("NO"); break;
				case 1: System.out.println("DIABOLICO"); break;
				case 2: System.out.println("ESOTERICO");
			}
			
		} while(true);
	}
}
