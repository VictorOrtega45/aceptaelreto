import java.util.Scanner;

public class Problema179 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, m, suma, resultado;
		
		do
		{
			n = sc.nextInt();
			
			if(n == 0) break;
			
			suma = 0;
			int[] energias = new int[n];
			
			for(int i = 0; i < n; i++)
			{
				suma += sc.nextInt();
				energias[i] = suma;
			}
			
			m = sc.nextInt();
			
			for(int i = 0; i < m; i++)
			{
				int ini = sc.nextInt() - 2;
				int end = sc.nextInt() - 1;
				
				if(ini < 0) resultado = energias[end];
				else resultado = energias[end] - energias[ini];
				
				System.out.println(resultado);
			}
			
		} while(true);
	}
}
