import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PalmerasEnLaNieve2 {
	
	
	static int getEntrada() {
		int numero = 0;
		boolean seguir=true;
		BufferedReader input  = new BufferedReader(new InputStreamReader(System.in));
		do{
			try{numero = Integer.parseInt(input.readLine());
				seguir=false;
			} catch(Exception NumberFormatException){}
		}while(seguir);
		return numero;
	}
	
	public static void main(String[] args) {
		/*
		 * 
		 * 
		 * 
		 */
		// Declaració variables
		int cantidadEntradas = 0;		long cantidadPalmeras = 0;
		int longitudFranja = 0;
		int respuesta = 0;				int contador = 0;
		int peso = 0;
		int pesoIndividual = 0;
		// Entrada
		cantidadEntradas = getEntrada();
		// Bucle programa
		for (int i=0;i<cantidadEntradas;i++) {
			do{peso = getEntrada();}while(peso<=0);
			do{cantidadPalmeras = getEntrada();}while(cantidadPalmeras<=0||cantidadPalmeras>100000);
		// Pes palmera individual
			for(int j=0;j<cantidadPalmeras;j++){
				do{pesoIndividual = getEntrada();}while(pesoIndividual<=0);
				// Si palmera aguanta el pes augmentem les palmeres en peu
				if (pesoIndividual>=peso)contador++; 
				longitudFranja++;
				if(longitudFranja>respuesta) respuesta=longitudFranja;
				if (contador>=5) {
					longitudFranja=0;
					contador=0;
				}			 
			}
			System.out.println(respuesta);
			respuesta = 0;
			longitudFranja = 0;
			contador = 0;
		}
	}
}
