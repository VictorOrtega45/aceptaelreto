import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author VíctorOrtega
public class Problema437 {
	public static void main(String[] args) {
		// Declaració variables
		int n = 0;	String entrada = "";
		int hora = 0;	int minut = 0;	int segon = 0;
		int hS = 0;	int mS = 0;	int sS = 0;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try {n = Integer.parseInt(br.readLine());}
		catch(Exception e) {}
		
		while(n>0) {
			hS = 24;
			mS = 60;
			sS = 60;
			
			try { entrada = br.readLine();}
			catch (Exception e) {}
			
			String[] parts = entrada.split(":");
			hora = Integer.parseInt(parts[0]);
			minut = Integer.parseInt(parts[1]);
			segon = Integer.parseInt(parts[2]);
			
			if(segon>0) mS--;
			if(segon>0||minut>0) hS--;
			
			sS = sS-segon;
			mS = mS-minut;
			hS = hS-hora;
			
			if(sS==60) sS = 0;
			if(mS==60) mS = 0;
			
			System.out.printf("%02d:%02d:%02d%n", hS, mS, sS);
			
			n--;
		}
		
	}
}
