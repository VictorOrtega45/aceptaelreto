import java.util.Scanner;
// @author VictorOrtega
public class Problema359 {

	// Main
	public static void main(String[] args) {
		// Declaració variables
		int n = 0;	int totalConchas = 0;	int totalMejillones = 0;
		Scanner sc = new Scanner(System.in);
		int entrada = 0;
		
		n = sc.nextInt();
		
		for (int i = n; i>0; i--) {
			totalConchas = 0;
			totalMejillones = 0;
			
			do {
				entrada = sc.nextInt();
				if (entrada==-1) break;
				totalMejillones+=entrada;
				totalConchas++;
			} while(true);
			
			if (totalConchas>totalMejillones) System.out.println("Timo");
			if (totalConchas==totalMejillones) System.out.println("Justo");
			if (totalConchas<totalMejillones)System.out.println("Suerte");
		}
		sc.close();
	}
}
