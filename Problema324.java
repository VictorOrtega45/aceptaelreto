import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
// @author V�ctor Ortega
public class Problema324 {
	/*
	 * 
	 * 
	 */
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	// Input per a int
	static int getEntrada() {
		int entrada = 0;
		try {entrada = Integer.parseInt(br.readLine());}
		catch (Exception e) {}
		return entrada;
	}
	
	public static void main(String[] args) {
		
	// Declaraci� variables
		int numEntradas = 0;
		long movimientos = 0;
		int numero = 0;
		String entrada = "";
		
		
		do {
			
			numEntradas = getEntrada();
			if (numEntradas==0) break;
			
			// Arrays
			int[] cafe = new int[numEntradas]; // Inici
			int[] movimiento = new int[numEntradas]; // Forma de moure
			int[] calculos = new int[numEntradas]; // C�lculs
			
			try{entrada = br.readLine();}
			catch (Exception e) {}
			
			String[] partir = entrada.split(" ");
			
			// Creaci� input
			for (int i=0; i<numEntradas;i++) {
				numero = i+1;
				cafe[i] = numero;
				calculos[i] = numero;
				movimiento[i] = Integer.parseInt(partir[i]);
			}
			
			// Algorisme
			movimientos = 0;
			int num = 0;
			int math = 0;
			do {
				
				// ???
				int j = 0;
				
				for (int i = 0; i<numEntradas; i++) {
					
					math = movimiento[i]-1; // C�lcul
					
					num = calculos[math];
					calculos[math] = i+1;
					
					
					
					
					
					
				}
					
					
				
				
				movimientos++;
			} while(!Arrays.equals(calculos, cafe));
			
			
			System.out.println(movimientos);
			
		} while(numEntradas!=0);
		
	}
}
