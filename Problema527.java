import java.util.Scanner;

public class Problema527 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, suma, zeros, res;
		
		String entrada;
		
		n = sc.nextInt();
		
		while(n > 0)
		{
			entrada = sc.next();
			suma = 0;
			zeros = 0;
			
			for(char c : entrada.toCharArray())
			{
				suma += (int) c - 48;
				if(c == '0') ++zeros;
			}			

			if(zeros > 1 & ((res = suma % 9) == 0))
			{
				System.out.println("COMPLETO");
				--n;
				continue;
			}
			
			if(zeros == 0) System.out.print("00");
			else if(zeros == 1) System.out.print("0");
			
			if(res != 0) System.out.println(9 - res);
			else System.out.println();
			
			--n;
		}
	}
}
