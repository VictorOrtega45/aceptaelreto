import java.util.Scanner;
// @author VictorOrtega
public class Problema337 {

	// Main
	public static void main(String[] args) throws java.io.IOException {
		// Declaració variables
		int n = 0;	int[] diente = new int[6]; boolean salida = true;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0) {
			
			salida = true;
			for(int i = 0; i<6; i++) {
				diente[i] = 0;
			}
			
			for (int i = 0; i<2; i++) {
				diente[0]+= sc.nextInt();
				diente[1]+= sc.nextInt();
				diente[2]+= sc.nextInt();
				diente[3]+= sc.nextInt();
				diente[4]+= sc.nextInt();
				diente[5]+= sc.nextInt();
			}
			
			int media = diente[0];
			for(int m : diente) {
				if(m!=media) {
					salida = false;
					break;
				}
			}
			
			if(salida)System.out.println("SI");
			else System.out.println("NO");
			
			n--;
		}
	}
}
