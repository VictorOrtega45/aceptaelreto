import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author VíctorOrtega
public class Problema145 {
	static StringBuilder salt = new StringBuilder("\n");
	static int match = 0;
	static String entrada = "";
	static int llargada = 0;
	static int index = 0;
	static char[] ch = new char[10000]; 
	static StringBuilder sb = new StringBuilder();
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	public static void main(String[] args) throws Exception {
		while((entrada = br.readLine())!=null) {
			match = 0;
			index = 0;
			ch = entrada.toCharArray();
			
			
			for(int i = 1; i<entrada.length(); i++) {
				if(ch[i]=='@') {
					index = i;
				}
				else if(ch[i]=='M') {
					for(int j = i-1; j>=index; j--) {
						if(ch[j]=='H') {
							ch[j] = ' ';
							ch[i] = ' ';
							match++;
							break;
						}
						else if(ch[j]=='h'||ch[j]=='m') {
							index = j;
							break;
						}
					}
				}
				else if(ch[i]=='m') {
					for(int j = i-1; j>=index; j--) {
						if(ch[j]=='h') {
							ch[i] = ' ';
							ch[j] = ' ';
							match++;
							break;
						}
						else if(ch[j]=='H'||ch[j]=='M') {
							index = j;
							break;
						}
					}
				}
			}
			sb.append(match).append(salt);
		}
		System.out.print(sb.toString());
	}
}
