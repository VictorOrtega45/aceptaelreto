import java.util.Scanner;

public class Problema345 
{
	static final int nine = 9, three = 3;
	static int[][] sudoku;
	
	private static boolean validar()
	{
		for(int i = 0; i < nine; i++)
		{
			for(int j = 0; j < nine; j++)
			{
				for(int k = j+1; k < nine; k++)
				{
					if(sudoku[i][j] == sudoku[i][k])
					{
						return true;
					}
				}
				for(int k = i+1; k < nine; k++)
				{
					if(sudoku[i][j] == sudoku[k][j])
					{
						return true;
					}
				}
			}
		}
		
		
		 for(int l=0;l<7;l+=3)
	    {
	        for(int k=0;k<7;k+=3)
	        {
	            memset(solve,false,sizeof(solve));
	        for(int i=k;i<k+3;i++)
	            for(int j=l;j<l+3;j++)
	            {
	                if(solve[sudoku[i][j]])
	                    solv=false;
	                solve[sudoku[i][j]]=true;
	            }
	        }
	    }
		 
		return false;
	}
	
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n;
		boolean incorrecto;
		
		n = sc.nextInt();
		
		while(n > 0)
		{
			sudoku = new int[nine][nine];
			
			incorrecto = false;
			
			for(int i = 0; i < nine; i++)
			{
				for(int j = 0; j < nine; j++)
				{
					sudoku[i][j] = sc.nextInt();
				}
			}
			
			incorrecto = validar();
			
			if(incorrecto) System.out.println("NO");
			else System.out.println("SI");
			
			
			--n;
		}
	}
}
