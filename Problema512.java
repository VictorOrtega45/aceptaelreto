import java.util.Scanner;

public class Problema512 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt(), a, b;
		
		while(n > 0)
		{
			a = sc.nextInt();
			b = sc.nextInt();
			
			System.out.println((a * 100) / (a +b));
			
			--n;
		}
	}
}
