import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
// @author V�ctor Ortega
public class Problema145b {
	public static void main(String[] args) throws java.io.IOException{
		// Declaraci� variables
		String entrada = "";
		int pareja = 0;
		Stack<Character> hombres = new Stack<Character>();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		while((entrada=br.readLine())!=null) {
			pareja = 0;
			hombres.clear();
			
			char[] c = entrada.toCharArray();
			
			for(int i = 0; i<entrada.length(); i++) {
				if(c[i]=='H') hombres.push(c[i]);
				else if(c[i]=='h') hombres.push(c[i]);
				else if(c[i]=='@') {
					hombres.clear();
				}
				else if(c[i]=='M') {
					if(!hombres.isEmpty()) {
						if(hombres.pop()=='H') {
							pareja++;
						}
						else {
							hombres.clear();
						}
					}	
				}
				else if(c[i]=='m') {
					if(!hombres.isEmpty()) {
						if(hombres.pop()=='h') {
							pareja++;
						}
						else {
							hombres.clear();
						}
					}		
				}
			}
			System.out.println(pareja);
		}
	}
}
