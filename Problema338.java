import java.util.Scanner;
import java.util.HashMap;

public class Problema338 
{
	public static void main(String[] args)
	{
		int n = 0;
		int memoria = 0;
		int entrada = 0;
		int copiados = 0;
		
		HashMap<Integer, Integer> examen = new HashMap<Integer, Integer>();
		Scanner sc = new Scanner(System.in);
		
		
		while(sc.hasNext())
		{
			n = sc.nextInt();
			memoria = sc.nextInt();
			copiados = 0;
			
			for(int i = n; i>=0; i--)
			{
				entrada = sc.nextInt();
				if(examen.get(entrada)==null) examen.put(entrada, 1);
				else examen.put(entrada, examen.get(entrada)+1);
			}
			
			for(int nu : examen.values())
			{
				copiados+=nu;
			}
			examen.clear();
			System.out.println(copiados);
		}
	}
}
