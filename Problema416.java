import java.util.Scanner;

public class Problema416 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n;
		String entrada;
		String[] fechas;
		boolean verdad;
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			verdad = false;
			
			fechas = new String[n];
			
			for(int i = 0; i < n; i++)
			{	
				entrada = sc.next();
				fechas[i] = entrada.substring(0, entrada.length()-5);
				for(int j = 0; j < i; j++)
				{
					if(fechas[i].equals(fechas[j]))
					{
						verdad = true;
					}
				}
			}
			 
			if(verdad) System.out.println("SI");
			else System.out.println("NO");
			
		} while(true);
	}
}
