import java.util.Scanner;

public class Problema529 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt(), max, conectados;
		
		
		
		String entrada;
		
		while(n > 0)
		{
			entrada = sc.next();
			max = 0;
			conectados = 0;
			
			for(char c : entrada.toCharArray())
			{
				if(c == 'I')
				{
					++conectados;
				}
				else
				{
					if(conectados > 0) --conectados;
					else ++max;
				}
				if(conectados > max)
				{
					max = conectados;
				}
			}
			
			System.out.println(max);
			
			--n;
		}
		
	}
}
