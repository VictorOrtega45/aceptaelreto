import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Problema243 
{
	static int salida = 0;
	// calcular divisores 
    static int calcularDivisors(int n) 
    { 
    	int contador = 2;
        int i = 2;
        int limit = (int) Math.floor(Math.sqrt(n));
    	while (i<=limit)
    	{
            if (n%i==0) contador=contador+2;
            i++;
    	}
    	if(limit*limit==n) contador--;
        return contador;
    } 
    
	public static void main(String[] args) throws Exception
	{
		int n = 0;
		StringBuilder sb = new StringBuilder();
		char salt = '\n';
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			n = Integer.parseInt(br.readLine());
			if(n==0) break;
			
			salida = n;
			
			
			sb.append(calcularDivisors(salida)).append(salt);
			
		} while(true);
		System.out.print(sb);
	}
}