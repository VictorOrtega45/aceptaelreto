// @author VíctorOrtega
public class Problema372b {
	public static void main(String[] args) throws Exception {
		int n = 0;
		char[] palabra = new char[40];
		char salt = '\n';
		int i = 0;
		int j = 0;
		int ascii = 0;
		int a; int b;	int c = 0;
		StringBuilder sb = new StringBuilder();
		
		n = System.in.read()-48;
		a = System.in.read();
		b = System.in.read();
		
		while(n>0) {
			i = 0;
			while((palabra[i]=(char) System.in.read())!='\n') i++;
			
			c = i-2;
			ascii = (int) palabra[0];
			if(ascii<91) {
				palabra[0] = Character.toLowerCase(palabra[0]);
				palabra[c] = Character.toUpperCase(palabra[c]);
			}
			
			j = c;
			while(j>=0) {
				sb.append(palabra[j]);
				j--;
			}
			sb.append(salt);
				
			n--;
		}
		System.out.print(sb.toString());
	}
}	