import java.util.Arrays;
import java.util.Scanner;

public class Problema507 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		int n,
			ini,
			fi,
			llargada;
		long
			max,
			entrada,
			resultado;
		long[] integrantes;
		
		do
		{
			n = sc.nextInt();
			max = sc.nextInt();
			
			if(n == 0 && max == 0) break;
			
			integrantes = new long[n];
			
			llargada = 0;
			
			for(int i = 0; i < n; i++)
			{
				entrada = sc.nextInt();
				if(entrada < max)
				{
					integrantes[llargada] = entrada;
					++llargada;
				}
			}
			
			Arrays.sort(integrantes, 0, llargada);
			
			fi = llargada-1;
			ini = 0;
			resultado = 0;
			
			while(fi != ini)
			{
				if(integrantes[fi] + integrantes[ini] > max)
				{
					--fi;
				}
				else 
				{
					resultado += fi - ini;
					++ini;
				}
			}
			
			System.out.println(resultado);
			
		} while(true);
	}
}
