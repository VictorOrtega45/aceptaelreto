import java.util.Scanner;
// @author V�ctor Ortega
public class Problema216 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	int tiempo[] = new int[3];
		int entrada = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		for (int i = n; i>0; i--) {
			entrada = sc.nextInt();
			
			for(int j = 0; j<3; j++) {
				tiempo[j] = 0;
			}
			
			while(entrada>0) {
				if(entrada-3600>=0) {
					entrada-=3600;
					tiempo[0]++;
					continue;
				}
				if(entrada-60>=0) {
					entrada-=60;
					tiempo[1]++;
					continue;
				}
				tiempo[2] = entrada;
				entrada = 0;
			}
			
			for(int j = 0; j<2; j++) {
				if(tiempo[j]<10) System.out.print("0");
				System.out.printf("%d:",tiempo[j]);
			}
			if(tiempo[2]<10) System.out.print("0");
			System.out.println(tiempo[2]);
			
			
		}
		sc.close();
	}
}
