import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class HolaSoy {
	public static void main(String[] args){
		/*
		 * El programa transforma un "Soy (nomUsuari)" a 
		 * "Hola, (nomUsuari)"
		 */
		// Declaració variables
		int entrada = 0;
		String nomEntrada = "";
		
		// Entrada
		Scanner sc = new Scanner(System.in);
		entrada = sc.nextInt();
		String[] nom = new String[(entrada+1)];
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// Algorisme
		for (int i=0;i<entrada;i++) {
			try {nomEntrada = br.readLine();}
			catch(Exception e){}
			nom[i] = nomEntrada.substring(4);
		}
		// Sortida
		for (int j=0;j<entrada;j++) {
			System.out.printf("Hola, %s%n",nom[j]);
		}
		sc.close();
	}
}
