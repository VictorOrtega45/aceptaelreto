import java.util.Scanner;
// @author V�ctor Ortega
public class PoliDivisibleNew {
	public static void main (String[] args) {
		// Declaraci� variables
		int N = 0;
		int D = 0;
		int llargadaN = 0;
		int llargadaActual = 0;
		int exponent = 0;
		int potenciaDeu = 0;
		boolean notPoliDivisible = false;
		int resultat = 0;
		int j = 0;
		int rango = 0;
		
		// Input
		Scanner sc = new Scanner(System.in);
		
		// Algorisme
		for (int i=0;i<2;i++) {
			N = sc.nextInt();
			D = sc.nextInt();
			llargadaN =  String.valueOf(N).length();
			llargadaActual = 2; // �s comen�a amb els 2 primers n�meros
			exponent = llargadaN-2;  // �s comen�a dividint els 2 primers n�meros
			if (D>=llargadaN) {
				while (exponent>=0) {
					potenciaDeu = (int) Math.pow(10, exponent); // �s c�lcula la pot�ncia
					resultat = N/potenciaDeu;	// �s c�lculen els dos primers d�gits
					if (resultat%llargadaActual!=0) { // Si qualsevol c�lcul t� un residu 
						exponent = 0;				  // �s surt del while
						notPoliDivisible = true;	  // El n�mero no �s polidivisible
				}
					exponent--;		   // S'agafen m�s n�mero treient un 0 de la potencia
					llargadaActual++;  // S'augmenta el n�mero de d�gits
				}
				if (notPoliDivisible==false) {
					System.out.println(N);
					rango = (int) Math.pow(10, (D-llargadaN))-1;
					while (D>llargadaN) {
						N = N* (int) Math.pow(10, (D-llargadaN));
						
						for(j=0;j<rango;j++) {			
							llargadaN =  String.valueOf(N).length();
							llargadaActual = 2; // �s comen�a amb els 2 primers n�meros
							exponent = llargadaN-2;  // �s comen�a dividint els 2 primers n�meros
							while (exponent>=0) {
								potenciaDeu = (int) Math.pow(10, exponent); // �s c�lcula la pot�ncia
								resultat = N/potenciaDeu;	// �s c�lculen els dos primers d�gits
								if (resultat%llargadaActual!=0) { // Si qualsevol c�lcul t� un residu 
									exponent = 0;				  // �s surt del while
									notPoliDivisible = true;	  // El n�mero no �s polidivisible
								}
								exponent--;		   // S'agafen m�s n�mero treient un 0 de la potencia
								llargadaActual++;  // S'augmenta el n�mero de d�gits
							}
							if (notPoliDivisible==false) System.out.println(N);
							N++;
							notPoliDivisible=false;
						}
						N=N-9;
					}
				}
			}
			System.out.println("---");
		}
		System.out.println("Se ha acabat el programa");
		sc.close();
	}
}
