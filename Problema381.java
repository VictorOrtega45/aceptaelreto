import java.util.Scanner;
// @author V�ctor Ortega
public class Problema381 {
	static int numero = 0;
	// Lista primos inferiores a 70
	static final int[] PRIMO = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67};
	
	static int calcularMcm(int m) {
		int potencia = 0;
		
		// Lista de potencias sobre num's primos
		if (m==2) potencia = 6;
		else if(m==3) potencia = 3;
		else if (m==5) potencia = 2;
		else if (m==7) potencia = 2;
		else potencia = 1;
		 
		do {
			if(numero%Math.pow(m, potencia)==0) { // Si el numero es divisible entre el primo y su potencia
				numero/= Math.pow(m,  potencia); // Dividimos
				return potencia; // Devolvemos la potencia
			}
			else potencia--; // Si no es divisible reducimos la potencia
		} while(potencia>0); // Si la potencia es 0 salimos (indicando que ese numero primo no es valido)
		
		return 0; // default
	}
	
	// Main
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0; 
		int mcm = 0;
		int index = 0;
		int resultadomcm = 0;
		
		Scanner sc = new Scanner(System.in); 
		
		do {
			int[] listaPotencia = new int[19]; // Array con las potencias de los 19 primos
			index = 0;
			n = sc.nextInt(); // Cogemos numero de entradas
			if (n==0) break; // Si la entrada es 0 salimos
			int[] entrada = new int[n]; // Definimos el tama�o de el array de entradas
			
			for(int i = 0; i<n; i++) {
				entrada[i] = sc.nextInt(); // Ponemos las entradas en un array de forma ordenada
			}
			
			// Procesamos el mcm de cada entrada por separado
			for(int j = 0; j<n; j++) {
				// Separamos el numero del array
				numero = entrada[j]; 
				
				// Hasta que el numero no sea =1 no paramos de calcular
				for(int i = 0; numero>1; i++) {	
					// ERROR
					resultadomcm = calcularMcm(PRIMO[i]); // Cogemos la potencia
					if(resultadomcm>listaPotencia[i]) {	// Si la potencia es m�s grande que la actual se substituye
						listaPotencia[i] = resultadomcm;
					}
					if(i>index) index = i; // Guardamos hasta que posicion del array ha llegado
				}	
			}
			
			mcm = 1; // mcm = 1 para evitar el 0 al multiplicar
			for(int i = 0; i<=index; i++) {	// Miramos las potencias hasta el index anterior
				/* Calculamos el mcm multiplicando los numeros primos 
				 * elevados a sus potencias entre ellos
				 */
				mcm*=Math.pow(PRIMO[i], listaPotencia[i]);
			}
			
			System.out.println(mcm); // Salida
			
		} while(true); // No salimos hasta que la entrada sea un '0'
		sc.close();
	}
}