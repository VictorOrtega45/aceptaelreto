import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author VíctorOrtega
public class Problema506 {
	
	static final char[][] h = new char[][] { new char[] { 'B', 'I', 'E', 'N', '\n' },
		new char[] { 'M', 'A', 'L', '\n' }};
				
	public static void main(String[] args) throws Exception {
		// Declaració variables
		int n = 0;	int izquierda = 0;	int derecha = 0;
		String entrada = "";
		
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		n = Integer.parseInt(br.readLine());
		while(n>0) {
			entrada = br.readLine();
			String[] parts = entrada.split(" / ");
			izquierda = Integer.parseInt(parts[0]);
			derecha = Integer.parseInt(parts[1]);
			
			if(izquierda>=derecha) sb.append(h[0]);
			else sb.append(h[1]);
			
			n--;
		}
		System.out.print(sb);
	}
}
