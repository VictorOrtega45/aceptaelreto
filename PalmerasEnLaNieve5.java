import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PalmerasEnLaNieve5 {
	
	public static int getEntrada() {
		BufferedReader input  = new BufferedReader(new InputStreamReader(System.in));
		int numero = 0;
			try{numero = Integer.parseInt(input.readLine());
			} catch(Exception NumberFormatException){System.out.print("Error.");}
			return numero;
			
	}
		
	
	public static void main(String[] args) {
		/*
		 * 
		 * 
		 * 
		 */
		// Declaració variables
		int cantidadEntradas = 0;
		
		// Entrada
		while(cantidadEntradas<=0)cantidadEntradas = getEntrada();
		int[] peso = new int[cantidadEntradas];
		int[] respuesta = new int[cantidadEntradas];
		
		
		// Bucle programa
		for (int i=0;i<cantidadEntradas;i++) {
			int cantidadPalmeras = 0;
			int longitudFranja = 0;
			int contador = 0;
			while(peso[i]<=0)peso[i] = getEntrada();
			while(cantidadPalmeras<=0||cantidadPalmeras>100000)cantidadPalmeras = getEntrada();
		
		// Pes palmera individual
			for(int j=0;j<cantidadPalmeras;j++){
				int pesoIndividual=0;
				while(pesoIndividual<=0)pesoIndividual = getEntrada();
		
				// Si palmera aguanta el pes augmentem les palmeres en peu
				if (pesoIndividual>=peso[i])contador++; 
				longitudFranja++;
				if(longitudFranja>respuesta[i]) respuesta[i]=longitudFranja;
				//if(longitudFranja>=j-cantidadPalmeras) break;
				 if (contador>=5) {
					longitudFranja=0;
					contador=0;
				 }			 
			}
		}
		for (int i=0;i<cantidadEntradas;i++) System.out.println(respuesta[i]);
	}
}
