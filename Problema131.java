import java.util.Scanner;
// @author V�ctor Ortega
public class Problema131 {
	public static void main(String[] args) {
		// Declaraci� variables
		int litrosPiscina1 = 0;	int litrosPiscina2 = 0;
		int litrosViaje1 = 0;	int litrosViaje2 = 0;
		int litrosPerdidos1 = 0; int litrosPerdidos2 = 0;
		int resultado1 = 0;	int resultado2 = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			litrosPiscina1 = sc.nextInt();
			litrosViaje1 = sc.nextInt();
			litrosPerdidos1 = sc.nextInt();
			litrosPiscina2 = sc.nextInt();
			litrosViaje2 = sc.nextInt();
			if(litrosPiscina1==0||litrosPiscina2==0) break;
			litrosPerdidos2 = sc.nextInt();
			
			
			litrosViaje1-=litrosPerdidos1;
			litrosViaje2-=litrosPerdidos2;
			
			if(litrosViaje1<1&&litrosViaje2<1) {
				System.out.println("EMPATE 0");
				continue;
			}
			resultado1 = (int)Math.ceil((double)litrosPiscina1/(double)litrosViaje1);
			resultado2 = (int)Math.ceil((double)litrosPiscina2/(double)litrosViaje2);
			
			
			if(resultado1<resultado2||resultado2<1) System.out.println("YO "+resultado1);
			else if(resultado2<resultado1||resultado1<1) System.out.println("VECINO "+resultado2);
			else System.out.println("EMPATE "+resultado1);
			
		} while(true);
	}
}
