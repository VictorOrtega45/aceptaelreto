import java.util.Scanner;

public class Problema150 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int n = 0,
			fila = 0,
			columna = 0,
			espai = 0;
		
		char m = ' ';
		
		do
		{
			n = sc.nextInt();
			m = sc.next().charAt(0);
			
			if(n == 0 && m == '0') break;
			
			columna = 2 * n -1;
			
			espai = n;
		
			for(int i = 0; i < columna; i++)
			{
				for(int j = 0; j < columna + (n - espai); j++)
				{
					if(j < espai-1) System.out.print(" ");
					else System.out.print(m);
				}
				System.out.println();
				if(i < n-1) --espai;
				else ++espai;
			}
			
			
			
		} while(true);
		
		
	}
}
