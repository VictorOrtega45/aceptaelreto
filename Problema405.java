import java.util.Scanner;

public class Problema405 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int 
			n,
			entrada,
			ini = 0;
		boolean repetido;
		String salida;
		
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			salida = "";
			repetido = false;
			
			do
			{
				entrada = sc.nextInt();
				if(entrada - n == 1)
				{
					if(!repetido)
					{
						ini = n;
					}
					repetido = true;
				}
				else
				{
					if(repetido)
					{
						salida += ini + "-" + n + ",";
					}
					else
					{
						salida += n + ",";
					}
					repetido = false;
				}
				
				n = entrada;
			} while(entrada != 0);
			System.out.println(salida.substring(0, salida.length()-1));
		} while(true);
	}

}
