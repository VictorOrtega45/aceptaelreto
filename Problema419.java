import java.util.Arrays;
import java.util.Scanner;

public class Problema419 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int pes, usuaris, i, j, viatges;
        Integer[] taula;

        do
        {
        	pes=sc.nextInt();
	        usuaris=sc.nextInt();
	        taula = new Integer[usuaris];
	        if(pes == 0 && usuaris ==0) break;
	       

			for(i=0; i<usuaris; i++)
			{
                taula[i]=sc.nextInt();
			}
            Arrays.sort(taula);
			i = 0;
			j = usuaris-1;
			viatges = 0;
	
			while(i<j)
			{
				if(taula[i] + taula[j] <= pes)
				{ 
					// comprova si podem posar dos usuaris en un viatge: el de m�s pes i el de menys disponible
					i++;
					j--;
				}
				else
				{
					j--; //si no es pot posar viatjar� nom�s usuari m�s pes 
				}
				viatges++;
			}
			if (i == j) viatges++; //el darrer usuari 
			System.out.println(viatges);
        } while(true);
    }   
}
