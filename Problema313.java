import java.util.Scanner;
// @author V�ctor Ortega
public class Problema313 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	int s = 0;	int c = 0;
		int resultado = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0) {
			s = sc.nextInt();
			c = sc.nextInt();
			resultado = s+c;
			
			if(resultado>=0) System.out.println("SI");
			else System.out.println("NO");
		n--;
		}
	}
}
