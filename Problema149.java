import java.util.Scanner;
// @author VíctorOrtega
public class Problema149 {
	public static void main(String[] args) {
		int n = 0;	int comparar = 0;	int max = 0; char s = '\n';
		StringBuilder sb = new StringBuilder();
		
		Scanner sc = new Scanner(System.in);
		
		
		while(sc.hasNext()) {
			n = sc.nextInt();
			max = 0;
			
			for(int i = n; i>0; i--) {
				comparar = sc.nextInt();
				if(comparar>max) max = comparar;
			}
			sb.append(max).append(s);
		}
		System.out.print(sb.toString());
	}	
}
