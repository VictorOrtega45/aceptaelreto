import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
// @author V�ctor Ortega
public class Problema302 {
	/*
	 * 
	 * 
	 */
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	static int getInt() {
		int entrada = 0;
		try {entrada = Integer.parseInt(br.readLine());}
		catch (Exception e) {}
		return entrada;
	}
	// Main
	public static void main(String[] args) {
		// Declaraci� variables
		int numEntradas = 0;	int llargada = 0;
		int entrada = 0;		int contador = 0;
		
		do {
			numEntradas = getInt();
			ArrayList<Integer> client = new ArrayList<Integer>();
			String[] sortida = new String[numEntradas];
			
			for (int i = 0; i<numEntradas; i++) {
				entrada = getInt();
				
				if(entrada!=0) client.add(entrada);
				else {
					Collections.sort(client);
					llargada = (client.size()-1);
					sortida[contador] = String.valueOf(client.get(llargada/2));
					client.remove(llargada/2);
					contador++;
				}
		}
		
			for (String i : sortida) {
				System.out.println(i);
			}
			
		
		} while(true);	
		
	}
}
