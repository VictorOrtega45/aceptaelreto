import java.util.Scanner;

public class Problema411 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		
		long n, m, res = 0;
		
		do 
		{
			n = sc.nextLong();
			if(n == 0) break;
			
			res = 0;
			do
			{
				m = sc.nextLong();
				if(m == 0) break;
				
				n -= m;
				if(n >= 0)
					++res;
			} while(true);
			System.out.println(res);
		} while(true);
	}
}
