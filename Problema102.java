import java.util.Scanner;

public class Problema102 
{
	
	static boolean comparar(char c, int diferencia)
	{
		switch(c)
		{
		case '�': case '�': case '�': case '�': case '�': case ' ':	
			return false;
		}
				
		int numero = (int) c-diferencia;
		if(numero<123) c = (char) numero;
		else 
		{
			c = (char) (97 + numero-123);
		}
		
		switch(c)
		{
		case 'a': case 'o': case 'i':
		case 'e': case 'u': return true;
		default: return false;
		}
	}
	
	public static void main(String[] args)
	{
		String entrada;
		int diferencia;
		int contador;
		
		Scanner sc = new Scanner(System.in);
		
		do 
		{
			entrada = sc.nextLine();
			
			char[] c = entrada.toCharArray();
			diferencia = (int) c[0] - 112;
			
			if(c.length==4)
			{	
				if((char) ((int)c[1]-diferencia)=='F')
					if((char) ((int)c[2]-diferencia)=='I')
						if((char) ((int)c[3]-diferencia)=='N')
							break;
			}
			
			c = entrada.toLowerCase().toCharArray();
					
			contador = 0;
			
			for(char ch : c)
			{
				if(comparar(ch, diferencia)) contador++;
			}
			
			System.out.println(contador);
		} while(true);
	}
}