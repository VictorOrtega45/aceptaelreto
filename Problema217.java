import java.util.Scanner;
// @author VíctorOrtega
public class Problema217 {
	public static void main(String[] args) {
		int n = 0;	char salt = '\n';
		StringBuilder sb = new StringBuilder();
		
		Scanner sc = new Scanner(System.in);
		
		do {
			n  = sc.nextInt();
			if(n==0) break;
			
			if(n%2==0) sb.append("DERECHA");
			else sb.append("IZQUIERDA");
			sb.append(salt);
			
		} while(true);
		System.out.print(sb);
	}
}
