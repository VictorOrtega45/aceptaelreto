import java.util.Scanner;
// @author VíctorOrtega
public class Problema320 {
	public static void main(String[] args) {
		// Declaració variables
		int n = 0;	int nieve = 0;	int palmeras = 0;
		int peso = 0;	int contador = 0;	int max = 0;
		int franja = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0) {
			contador = 0;
			nieve = sc.nextInt();
			palmeras = sc.nextInt();
			
			
			contador = 0;
			max = 0;
			franja = 0;
			for(int i = palmeras; i>0; i--){
				franja++;
				peso = sc.nextInt();
				if(peso>=nieve)contador++;
				if(franja>max) max = franja;
				if(contador==5) {
					contador = 0;
					franja = 0;
				}
			}
			System.out.println(max);
		
			n--;
		} 
		sc.close();
	}
}
