import java.util.HashMap;
import java.util.Scanner;

public class Problema414 
{
	public static void main(String[] args)
	{
		int n,
			entrada,
			entradaTemp,
			raiz;
		double sqrt;
		long salida;
		
		Scanner sc = new Scanner(System.in);
		HashMap<Integer, Integer> h = new HashMap<>();
		
		n = sc.nextInt();
		
		while(n > 0)
		{
			entrada = sc.nextInt();
			
			entradaTemp = entrada;
			raiz = (int) Math.sqrt(entrada);
			
			while(entradaTemp > 1)
			{
				if(entradaTemp % raiz == 0)
				{
					entradaTemp /= raiz;
					if(raiz % 2 != 0)
					{
						if(h.containsKey(raiz)) h.put(raiz, h.get(raiz)+1);
						else h.put(raiz, 1);
					}
				}
				else --raiz;
			}
			
			salida = 0;
			for(Integer i : h.keySet())
			{
				salida += Math.pow(i, h.get(i));
			}
			
			System.out.println(salida);
			h.clear();
			n--;
		}
	}
}
