import java.util.Scanner;

public class Problema523 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		while(n > 0)
		{
			String entrada = sc.next();
			
			String num = entrada.substring(0, 4);
			String letras = entrada.substring(4, 7);
			
			String matriculaP = letras + num;
			int min = 0;
			int max = 0;
			
			do
			{
				entrada = sc.next();
				if(entrada.equals("0")) break;
			
				num = entrada.substring(0, 4);
				letras = entrada.substring(4, 7);
				
				String matricula = letras + num;
				
				int compare = matriculaP.compareTo(matricula);
				
				if(compare > 0) ++min;
				else ++max;
				
			} while(true);
			
			System.out.println(min + " " + max);
			
			--n;
		}
	}
}
