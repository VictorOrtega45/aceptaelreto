import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema154 
{
	public static void main(String[] args) throws Exception
	{
		String entrada = "";			
		int numero;
		char[] letra = new char[3];
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do
		{
			entrada = br.readLine();
			if(entrada.equals("9999 ZZZ")) break;
			
			String[] parts = entrada.split(" ");
			
			numero = Integer.parseInt(parts[0]);
			
			
			if(numero==9999)
			{
				letra = parts[1].toCharArray();
				if(letra[2] == 'Z')
				{
					letra[2] = 'B';
					if(letra[1] == 'Z')
					{
						letra[1] = 'B';
						switch(letra[0])
						{
						case 'D': case 'T': case 'H': case 'N':
							letra[0] = (char) (1+(int)letra[0]);
							default: letra[0] = (char) (1+(int)letra[0]);
						}
					}
					else
					{
						switch(letra[1])
						{
						case 'D': case 'T': case 'H': case 'N':
							letra[1] = (char) (1+(int)letra[1]);
							default: letra[1] = (char) (1+(int)letra[1]);
						}
					}
				}
				else 
				{
					switch(letra[2])
					{
					case 'D': case 'T': case 'H': case 'N':
						letra[2] = (char) (1+(int)letra[2]);
						default: letra[2] = (char) (1+(int)letra[2]);
					}	
				}
				System.out.println("0000 " + letra[0]+letra[1]+letra[2]);
			}
			else 
			{
				numero++;
				System.out.println(String.format("%04d", numero) + " " + parts[1]);
			}
			
		} while(true);
	}
}
