import java.util.Arrays;
import java.util.Scanner;

public class Problema478b 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n,temp = 0;
		long suma, m;
		
		do
		{
			n = sc.nextInt();
			
			if(n == 0) break;
			
			long[] conjuros = new long[n];
			
			suma = 0;
			for(int i = 0; i < n; i++)
			{
				conjuros[i] = sc.nextInt();
				suma += conjuros[i];
			}
			
			m = sc.nextLong();
			
			suma -= m;
			
			for(int i = 0; i < n; i++)
			{
				temp = Arrays.binarySearch(conjuros, suma - conjuros[i]);
				if(temp > 0)
				{
					System.out.println(conjuros[i] + " " + conjuros[temp]);
					break;
				}
			}
			
			
		} while(true);
	}
}
