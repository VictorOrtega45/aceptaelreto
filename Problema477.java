import java.util.Arrays;
import java.util.Comparator;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class Arma
{
	int inocente;
	int hostil;
	int posicio;
}

public class Problema477 {
	public static void main(String[] args) throws Exception
	{
		int vitalidad = 0,
				nArmas = 0,
				i = 0;
		String salida = "",
				entrada = "";
		final char SALT = '\n',
					ESPAI = ' ';
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do
		{
			vitalidad = Integer.parseInt(br.readLine());
			if(vitalidad == 0) break;
			
			nArmas = Integer.parseInt(br.readLine());
			
			Arma[] armas = new Arma[nArmas];
			
			
			for(int k = 0; k < nArmas; k++)
			{
				entrada = br.readLine();
				String[] parts = entrada.split(" ");
				armas[k] = new Arma();
				armas[k].inocente = Integer.parseInt(parts[0]);
				armas[k].hostil = Integer.parseInt(parts[1]);
				armas[k].posicio = k+1;
			}
			
			Arrays.sort(armas, new Comparator<Arma>() {
				public int compare(Arma a1, Arma a2)
				{
					int a = Integer.compare(a1.inocente, a2.inocente);
					
					if(a == 0) 
					{
						return -Integer.compare(a1.hostil, a2.hostil);
						
					}
					else return a;
				}
			});
			
			for (i = 0; i < armas.length && vitalidad > 0; i++)
			{
				vitalidad -= armas[i].hostil;
			}
			if(vitalidad > 0) 
			{
				sb.append("MUERTE ESCAPA").append(SALT);
			}
			else
			{
				--i;
				for(int j = 0; j < i; j++)
				{
					 sb.append(armas[j].posicio).append(ESPAI);
				}
				sb.append(armas[i].posicio).append(SALT);
			}
		} while(true);
		System.out.print(sb.toString());
	}
}
