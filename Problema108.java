import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
// @author V�ctor Ortega
public class Problema108 {
	
	static String getLletra(char lletra){
		String text="";
		switch (lletra) {
			case 'D': text="DESAYUNOS";
			break;
			case 'A': text="COMIDAS";
			break;
			case 'M': text="MERIENDAS";
			break;
			case 'I': text="CENAS";
			break;
			case 'C': text="COPAS";
		}
		return text;
	}

	public static void main(String[] args) {
		/*
		 * 
		 * 
		 */
		// Declaraci� variables
	 	char entrada = ' ';
		float entradaValor = 0f; float valorTotal = 0f;
		String textMax = "";	 int contador=0;
		String textMin = "";	 float valorMax = 0f;
		float valorMin = 0f;	 int respuesta = 0;
		float media = 0f;		 String sRespuesta = "NO";
		float comidas = 0f;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try {	entrada = br.readLine().charAt(0);
			entradaValor = Float.valueOf(br.readLine());
		}
		catch(IOException e) {}
		textMax=getLletra(entrada);
		textMin=getLletra(entrada);
		valorTotal+=entradaValor;
		contador++;                       // valorTotal/contador==mitjanaAritm�tica
		valorMax=entradaValor;
		valorMin=entradaValor;
			
		// Algorisme
		do {
			try {	entrada = br.readLine().charAt(0);
				entradaValor = Float.valueOf(br.readLine());
			}
			catch(IOException e) {}
			if (entrada=='A')comidas=entradaValor;
			if (entradaValor>valorMax) {
				valorMax=entradaValor;
				textMax=getLletra(entrada);
			}
			else if (entradaValor<valorMin) {
				if(entradaValor!=0){valorMin=entradaValor;
				textMin=getLletra(entrada);
				}
			}
			else if (entradaValor==valorMax) respuesta=1;
			else if (entradaValor==valorMin) respuesta=2;
			valorTotal+=entradaValor;
			contador++;                       // valorTotal/contador==mitjanaAritm�tica
			} while(entrada!='N'&&entradaValor!=0);
		media=valorTotal/contador;
		if (media<comidas)sRespuesta="SI";
		
		if(respuesta==1||contador<4)System.out.print(textMax+"#"+"EMPATE#"+sRespuesta);
		else if(respuesta==2)System.out.print("EMPATE#"+textMin+"#"+sRespuesta);
		else if(valorMax==0)System.out.print("EMPATE#EMPATE#NO");
		else if(respuesta==0)System.out.print(textMax+"#"+textMin+"#"+sRespuesta);
	}
}
