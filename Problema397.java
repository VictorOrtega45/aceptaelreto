import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema397 {
	public static void main(String[] args) {
		// Declaraci� variables
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		String entrada = "";
		int multiplo = 0;
		
		int n = sc.nextInt();
		
		while(n>0) {
			
			try { entrada = br.readLine();}
			catch (Exception e) {}
			multiplo = 0;
			
			for(char c : entrada.toCharArray()) {
				multiplo+=Character.valueOf(c)-48;
			}
			
			if(multiplo%3==1) System.out.println("NO");
			else System.out.println("SI");
			
			n--;
		}
		
	}
}
