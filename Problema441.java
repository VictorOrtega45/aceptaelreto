import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema441 {
	public static void main(String[] args) {
		// Declaraci� variables
		String entrada = "";
		char c = ' ';
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			int contador = 0;
			boolean sumar = true;
			try{entrada = br.readLine();}
			catch (Exception e) {}
			
			if (entrada.equals("")) break;
			
			char[] numSalida = entrada.toCharArray();
			
			for(int i = entrada.length()-1; i>=0; i--) {
				if (contador==3) {
					contador = 0;
					continue;
				}
				
				if (sumar) {
					sumar = false;
					c = entrada.charAt(i);			
					int a = ((int) c) + 1;
		
					if (a==58) {
						c = (char) 48;
						sumar = true;
					}
					else c = (char) a;
					
					numSalida[i] = c;
					contador++;
				}
			}
			
			if (numSalida[0]=='0') System.out.print(1);
			if (contador==3) System.out.print(".");
			for(char ch : numSalida) {
				System.out.print(ch);
			}
			System.out.println();
		} while(true);
		
		
	}
}
