import java.util.Scanner;
// @author VíctorOrtega
public class Problema368 {
	public static void main(String[] args) {
		// Declaració variables
		double numHuevos = 0;	double capacidadHuevos = 0;
		double veces = 0;
		
		Scanner sc = new Scanner(System.in);
		
		do {
			numHuevos = sc.nextDouble();
			capacidadHuevos = sc.nextDouble();
			
			if(capacidadHuevos==0) break;
			
			veces = (int) Math.ceil(numHuevos/capacidadHuevos);
			
			System.out.printf("%.0f%n", veces*10);
			
		} while(true);
	
	}
}
