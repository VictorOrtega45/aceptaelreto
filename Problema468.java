import java.util.Scanner;

public class Problema468 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, max;	
		
		while(sc.hasNext())
		{
			n = sc.nextInt();
			
			int primeraEntrada = sc.nextInt();
			max = primeraEntrada;
			
			for(int i = 1; i < n; i++)
			{
				int num = sc.nextInt();
				if(max < num)
				{
					max = num;
				}
			}
			
			System.out.println(max - primeraEntrada);
		}
	
	}
}
