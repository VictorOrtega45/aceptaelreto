import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema370 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	String entrada = "";	int min = 0;
		int part1 = 0;	int part2 = 0;	int max = 0;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try { n = Integer.parseInt(br.readLine());}
		catch (Exception e) {}
		
		while(n>0) {
			try {entrada = br.readLine();}
			catch (Exception e) {}
			
			String[] parts = entrada.split("-");
			part1 = Integer.valueOf(parts[0]);
			part2 = Integer.valueOf(parts[1]);
			
			if(part1>part2) {
				min = part2;
				max = part1;
			}
			else {
				min = part1;
				max = part2;
			}
			
			if(min%2==0&&max==min+1) System.out.println("SI");
			else System.out.println("NO");
			
			n--;
		}
		
	}
}
