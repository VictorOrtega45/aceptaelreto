import java.util.ArrayList;
import java.util.Scanner;

public class Problema312 
{
	private static ArrayList<Integer> list = new ArrayList<>();
	
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, min, suma;
		
		do
		{
			n = sc.nextInt();
			if(n != 0)
			{
				list.add(n);
				min = n;
				do
				{
					n = sc.nextInt();
					if(n == 0) break;
					list.add(n);
					if(n < min) min = n;
				} while(true);
				min = calculateLCM(min);
			}
			else break;
			
			suma = 0;
			for(int i = 0; i < list.size(); i++)
			{
				suma += list.get(i) / min; 
			}
			
			System.out.println(suma);
			list.clear();
		} while(true);
	}
	private static int calculateLCM(int min)
	{
		for(int i = 0; i < list.size(); i++)
		{
			if(list.get(i) % min != 0)
			{
				return calculateLCM(min - 1);
			}
		}
		return min;
	}
}
