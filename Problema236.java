import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Problema236 
{
	public static void main(String[] args) throws Exception
	{
		long base;
		int multiplicador;
		int veces;
		long resultado;
		String entrada;
		final char salt = '\n';
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			entrada = br.readLine();
			String[] parts = entrada.split(" ");
			base = Integer.parseInt(parts[0]); 
			if(base==0) break;
			multiplicador = Integer.parseInt(parts[1]);
			veces = Integer.parseInt(parts[2]);
			
			resultado = base;
			
			for(int i = 0; i<veces-1; i++)
			{
				base*= multiplicador;
				resultado+= base;
			}
			
			sb.append(resultado).append(salt);
			
		} while(true);
		System.out.print(sb);
	}
}
