import java.util.Scanner;
// @author V�ctor Ortega
public class Problema191 {
	public static void main(String[] args) {
		// Declaraci� variables
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();	int cantidad = 0;
		int capacidad = 0;	int diferencia = 0;
		int capacidadTotal = 0;	int diferenciaTotal = 0;
		
		while(n>0) {
			cantidad = sc.nextInt();
			capacidad = sc.nextInt();
			diferencia = sc.nextInt();
			
			capacidadTotal = cantidad*capacidad;
			
			diferenciaTotal = 0;
			for(int i = cantidad-1; i>0; i--) {
				diferenciaTotal+=diferencia*i;
			}
			
			System.out.println(capacidadTotal-diferenciaTotal);
			
			n--;
		}
	}
}
