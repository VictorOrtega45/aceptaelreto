import java.util.Arrays;
import java.util.Scanner;

public class Problema458 
{
	public static void main(String[] args)
	{
		int n;
		
		Scanner sc = new Scanner(System.in);
		
		do 
		{
			n = sc.nextInt();
			
			if(n == 0) break;
			
			long[] numeros = new long[n];
			
			for(int i = 0; i < n; i++) numeros[i] = sc.nextLong();
			
			Arrays.sort(numeros);
			
			if(numeros[0] * numeros[1] > 
				numeros[numeros.length-1] * numeros[numeros.length-2])
				System.out.println(numeros[0] * numeros[1]);
			else
				System.out.println(numeros[numeros.length-1] * numeros[numeros.length-2]);
			
		} while(true);
	}
}
