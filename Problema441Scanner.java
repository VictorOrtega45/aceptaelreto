import java.util.Scanner;
// @author V�ctor Ortega
public class Problema441Scanner {
	public static void main(String[] args) {
		// Declaraci� variables
		int contador = 0;
		boolean sumar = true;
		Scanner sc = new Scanner(System.in);
		String entrada = "";	
		
		while (sc.hasNext()) {
			sumar = true;
			contador = 0;
			entrada = sc.nextLine();
			char[] a = entrada.toCharArray();
			
			for(int i = a.length-1;i>=0;i--) {
				if (contador==3) {
					contador = 0;
					continue;
				}
				if(sumar) {
					int temp = (int) a[i]+1;
					a[i] = (char) temp;
					if(a[i]==58) {
						a[i] = 48;
						sumar = true;
					}
					else sumar = false;
				}
				
				contador++;
			}
			if(a[0]=='0') {
				System.out.print("1");
				if(contador==3) System.out.print(".");
			}
			
			for(char c : a) {
				System.out.print(c);
			}
			System.out.println();		
		}
	}
}