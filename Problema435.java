import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema435 
{
	static int[] contador = new int[10];
	static int avg;
	private static boolean esSubnormal()
	{
		avg = contador[0];
		
		if(contador[0]!=avg) return false;
		if(contador[1]!=avg) return false;
		if(contador[2]!=avg) return false;
		if(contador[3]!=avg) return false;
		if(contador[4]!=avg) return false;
		if(contador[5]!=avg) return false;
		if(contador[6]!=avg) return false;
		if(contador[7]!=avg) return false;
		if(contador[8]!=avg) return false;
		if(contador[9]!=avg) return false;
		
		return true;
	}
	public static void main(String[] args) throws Exception
	{
		String entrada;
		char[] caracter;
		int llargada, numero;
		final char[] sub = {'s', 'u', 'b', 'n', 'o', 'r', 'm', 'a', 'l', '\n'};
		final char[] nor = {'n', 'o', ' ', 's', 'u', 'b', 'n', 'o', 'r', 'm', 'a', 'l', '\n'};
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		
		while((entrada = br.readLine()) != null)
		{
			contador[0] = 0; contador[1] = 0; contador[2] = 0; contador[3] = 0;
			contador[4] = 0; contador[5] = 0; contador[6] = 0; contador[7] = 0;
			contador[8] = 0; contador[9] = 0;
			llargada = entrada.length();
			caracter = entrada.toCharArray();
			for(int i = 0; i<llargada; i++)
			{
				numero = -48 + (int) caracter[i];
				contador[numero]++;
			}
			
			if(esSubnormal()) sb.append(sub);
			else sb.append(nor);
			
				
		}
		System.out.print(sb);
	}
}
