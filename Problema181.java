import java.io.BufferedReader;
import java.io.InputStreamReader;

class Posicion
{
	int valor;
	boolean esTransitable;
	
	Posicion(boolean esTransitable)
	{
		this.valor = 0;
		this.esTransitable = esTransitable;
	}
}

public class Problema181 
{
	public static void main(String[] args) throws Exception
	{
		// Declaració variables
		int n, iTemp;
		Posicion[][] posiciones;
		String entrada;
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do
		{
			n = Integer.parseInt(br.readLine());
			if(n == 0) break;
			
			posiciones = new Posicion[n][n];
			
			for(int i = n-1; i>=0; i--)
			{
				entrada = br.readLine();
				for(int j = 0; j<n; j++)
				{
					if(entrada.charAt(j) == 'X') 
					{
						posiciones[i][j] = new Posicion(false);
					}
					else
					{
						posiciones[i][j] = new Posicion(true);
					}
				}
			}
			
			posiciones[0][0].valor = 1;
			for(int i = 0; i<n; i++)
			{
				iTemp = i;
				for(int j = 0; j<=i; j++)
				{
					if(posiciones[iTemp][j].esTransitable)
					{
						if(iTemp != 0)
						{
							if(posiciones[iTemp-1][j].esTransitable)
							{
								posiciones[iTemp][j].valor+= posiciones[iTemp-1][j].valor;
							}
						}
						if(j != 0)
						{
							if(posiciones[iTemp][j-1].esTransitable)
							{
								posiciones[iTemp][j].valor+= posiciones[iTemp][j-1].valor;
							}
						}
					}
					iTemp--;
				}
			}
			
			// Falta mitad

			
			System.out.println(posiciones[n-1][n-1].valor);
		} while(true);
	}
}
