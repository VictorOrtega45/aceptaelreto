import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author VíctorOrtega
public class Problema145c {
	public static void main(String[] args) throws java.io.IOException {
		String entrada = "";
		int match = 0;
		int llargada = 0;
		boolean salir = false;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		while((entrada=br.readLine())!=null) {
			match = 0;
			llargada = entrada.length();
			
			char[] ch = entrada.toCharArray();
			
			
			for(int i = 0; i<llargada; i++) {
				salir = false;
				if(ch[i]=='H') {
					for(int j=i+1; j<llargada; j++) {
						switch(ch[j]) {
						case '@': 
						case 'h': 
						case 'm': salir = true; break; 	
						case 'M': match++; ch[j] = ' ';
						}
						if(salir) break;
					}	
				}
				else if(ch[i]=='h') {
					for(int j=i+1; j<llargada; j++) {
						switch(ch[j]) {
						case '@': 
						case 'H': 
						case 'M': salir = true; break; 	
						case 'm': match++; ch[j] = ' ';
						}
						if(salir) break;
					}	
				}
			}
			System.out.println(match);
		}
	}
}