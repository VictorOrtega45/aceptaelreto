import java.util.Scanner;
// @author VictorOrtega
public class Problema158 {
	/*
	 * Mario se encuentra ante el castillo final. Puede verlo desde lo alto del muro en el que se encuentra. En breve podr� entrar en la C�mara de Koopa, enfrentarse (y vencer) al monstruo final y salvar a la princesa.

Sin embargo, tiene ante s� una serie de muros que tendr� que sobrepasar. Para eso, saltar� desde el primero de ellos, donde se encuentra, al siguiente, y desde �l al siguiente, y as� sucesivamente hasta llegar al �ltimo.

La pregunta que nos hacemos es, �cu�ntos de estos saltos ser�n hacia arriba y cu�ntos hacia abajo? Mario realiza un salto hacia arriba cuando tiene que alcanzar un muro que est� por encima de �l, y hacia abajo cuando tiene que alcanzar un muro que est� por debajo.

Entrada
El primer valor de la entrada es un n�mero que indica la cantidad de casos de prueba a evaluar. Cada caso de prueba comienza con un entero mayor que cero y no mayor que 109 que indica el n�mero de muros del escenario (recuerda que Mario se encuentra situado en la parte de arriba del primero). A continuaci�n se proporciona la serie de enteros que indican la altura de cada uno de ellos.

Salida
Para cada caso de prueba se mostrar� una l�nea en la que aparecer�n dos enteros, uno con los saltos hacia arriba y otro con los saltos hacia abajo, separados por un espacio.
	 */

	// Main
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	int abajo;	int arriba;
		int cantidadMuros = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		for (int i = n; i>0; i--) {
			arriba = 0;
			abajo = 0;
			
			cantidadMuros = sc.nextInt();
			int[] muros = new int[cantidadMuros+1];
			
			for (int j = 0; j<cantidadMuros; j++) {
				muros[j] = sc.nextInt();
			}
			
			muros[cantidadMuros] = muros[cantidadMuros-1];
			
			for (int j = 0; j<cantidadMuros; j++) {
				if (muros[j]>muros[j+1]) abajo++;
				else if(muros[j]<muros[j+1]) arriba++;
			}
			
			System.out.printf("%d% d%n", arriba, abajo);
		}
		sc.close();
	}
}
