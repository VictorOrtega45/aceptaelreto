import java.util.Scanner;
// @author V�ctor Ortega
public class Problema344 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	int n2 = 0; String entrada = "";
		int h = 0;	int m = 0;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		for(int i = n; i>0; i--) {
			h = 0;
			m = 0;
			n2 = sc.nextInt();
			for(int j = n2; j>0; j--) {
				entrada = sc.next();
				for(char c : entrada.toCharArray()) {
					if(c=='H') h++;
					else if(c=='M') m++;
				}
			}
			if(h==m) System.out.println("POSIBLE");
			else System.out.println("IMPOSIBLE");
		}
	}
}
