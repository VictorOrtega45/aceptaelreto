import java.util.Scanner;

public class Problema121 
{
	public static void main(String[] args)
	{
		long dar;
		long recibir;
		long  caramelos;
		long total;
		
		Scanner sc = new Scanner(System.in);
		
		do 
		{
			dar = sc.nextLong();
			if(dar == 0) break;
			recibir = sc.nextLong();
			caramelos = sc.nextLong();
			
			if(caramelos>=dar && recibir>=dar)
			{
				System.out.println("RUINA");
				continue;
			}
			else if(recibir == 0)
			{
				System.out.println(caramelos + " " + caramelos);
				continue;
			}
			
			total = caramelos;
			while(caramelos>=dar)
			{
				total+=recibir*(caramelos/dar);
				caramelos = (caramelos%dar) + recibir*(caramelos/dar);
				
			}

		
			System.out.println(total + " " + caramelos);
		} while(true);
	}
}