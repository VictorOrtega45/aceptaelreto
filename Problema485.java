import java.util.Scanner;

public class Problema485 
{
	public static void main(String[] args)
	{
		int n,
			suma;
		int[] rutas = new int[100];
		String salida;
		
		Scanner sc = new Scanner(System.in);
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			salida = "";
			suma = 0;
			for(int i = 0; i < n; i++)
			{
				rutas[i] = sc.nextInt();
				
				suma += rutas[i];
			}
			
			for(int i = 0; i < n; i++)
			{
				salida += " " + suma;
				suma -= rutas[i];
			}
			
			System.out.println(salida.trim());
			
		} while(true);
	}
}
