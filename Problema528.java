import java.util.Scanner;

public class Problema528 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, index;
		
		long enteros, decimales, total;
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			total = 0;
			enteros = 0;
			decimales = 0;
			double[] nums = new double[n];
			
			for(int i = 0; i < n; i++)
			{
				String entrada = sc.next();
				nums[i] = Double.parseDouble(entrada);
			}
			
			index = 1;
			
			for(int i = 0; i < n; i++)
			{
				if(nums[i] == 0 || nums[i] == 1)
				{
					total += nums.length - index;
					++index;
				}
				else if(nums[i] > 1) ++enteros;
				else ++decimales;
			}
			
			total += enteros * (enteros - 1) / 2;
			total += decimales * (decimales - 1) / 2;
			
			System.out.println(total);
		} while(true);
	}
}
