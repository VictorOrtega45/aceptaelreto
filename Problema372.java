import java.util.Scanner;
// @author VíctorOrtega
public class Problema372 {
	public static void main(String[] args) {
		// Declaració variables
		int n = 0;	String entrada = "";
		int i = 0;	boolean caps = false;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n>0) {
			entrada = sc.next();
			caps = false;
			
			i = entrada.length();
			char[] sortida = new char[i];
			i--;
			
			if(91>(int)entrada.charAt(0)) caps = true;
			
			for(char c : entrada.toCharArray()) {
				sortida[i] = c;
				i--;
			}
			if(caps) {
				sortida[0] = (char) ((int)sortida[0]-32);
				int calcul = entrada.length()-1;
				sortida[calcul] = (char) ((int)sortida[calcul]+32);
			}
			
			for(char c : sortida) {
				System.out.print(c);
			}
			System.out.println();
			
			
			n--;
		}
	}
}
