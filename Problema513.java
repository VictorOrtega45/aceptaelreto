import java.util.Scanner;

public class Problema513 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		long n, sum, temp, res;
		
		while(sc.hasNext())
		{
			n = sc.nextInt();
			if(n == 0) continue;
			
			sum = sc.nextInt();
			System.out.print(sum);
			for(int i = 2; i <= n; i++)
			{
				temp = sc.nextInt();
				res = temp * i - sum;
				sum += res;
				System.out.print(" " + res);
			}
			System.out.println();
		}
	}
}
