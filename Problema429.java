import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;

public class Problema429
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n, m, temp;
		
		boolean posible = false;
		
		LinkedList<Integer> list = new LinkedList<>();
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			for(int i = 0; i < n; i++)
			{
				list.add(sc.nextInt());
			}
			
			Collections.sort(list, new Comparator<Integer>() {
				@Override
				public int compare(Integer o1, Integer o2) {
					return - Integer.compare(o1, o2);
				}
			});
			
			m = sc.nextInt();
			
			for(int i = 0; i < m; i++)
			{
				temp = sc.nextInt();
				posible = false;
				for(int j = 0; j < n; j++)
				{
					if(list.get(j) >= temp)
					{
						int res = list.get(j) - temp;
						list.remove(j);
						
						if(res != 0) 
						{
							if(list.size() > 0)
							{
								int length = list.size();
								for(int k = 0; k < length; k++) 
								{
									if(list.get(k) < res) list.add(k, res);
								}
								if(length == list.size()) list.add(res);
							}
							else 
								list.add(res);		
						}
							
						posible = true;
						break;
					}
				}
			}
			
			if(posible) System.out.println("SI");
			else System.out.println("NO");
			
			list.clear();
		} while(true);
	}
}