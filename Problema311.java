import java.util.PriorityQueue;
import java.util.Scanner;


class Paciente implements Comparable<Paciente>
{
	String nombre;
	int valor;
	int posicion;
	
	public Paciente(String nombre, int valor, int posicion)
	{
		this.nombre = nombre;
		this.valor = valor;
		this.posicion = posicion;
	}

	@Override
	public int compareTo(Paciente p2) {
		int x = Integer.compare(p2.valor, valor);
		if(x == 0) return Integer.compare(posicion, p2.posicion);
		return x;
	}
}

public class Problema311 
{	
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		int n = 0;
		
		char evento;
		String nombre;
		int valor;
		
		do
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			PriorityQueue<Paciente> pacientes = new PriorityQueue<>();
			
			for(int i = 0; i < n; i++)
			{
				evento = sc.next().charAt(0);
				
				if(evento == 'I')
				{
					nombre = sc.next();
					valor = sc.nextInt();
					pacientes.add(new Paciente(nombre, valor, i));
				}
				else
				{
					System.out.println(pacientes.poll().nombre);
				}
			}
			System.out.println("----");
			
		} while(true);
	}
}
