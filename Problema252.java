import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema252 
{
	public static void main(String[] args) throws Exception
	{
		String entrada;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			entrada = br.readLine();
			
			if(entrada.equals("XXX")) break;
			
			entrada = entrada.replace(" ", "");
			entrada = entrada.toLowerCase();
			
			if(entrada.equals(new StringBuilder(entrada).reverse().toString())) 
				System.out.println("SI");
			else System.out.println("NO");
			
		} while(true);
	}
}
