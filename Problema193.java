import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author VictorOrtega
public class Problema193 {
	static final char[] PARES = {'0','2','4','6','8'};
	public static void main(String[] args) {
		// Declaració variables
		int largada = 0;	int n = 0;	int m = 0;	int resultado = 0; boolean todoImpar = true;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			try {char[] entrada = br.readLine().toCharArray();
			if(entrada[0]=='0') break;
			largada = entrada.length;
			
			if(entrada[largada-1]=='0') {
				System.out.println("NO");
				continue;
			}
			todoImpar = true;
		
			n = Integer.parseInt(String.valueOf(entrada));
			
			char[] entrada2 = new char[largada];
			
			
			for(int i = 0, j = largada-1;i<largada; i++) {
				entrada2[i] = entrada[j-i];
			}
			
			m = Integer.parseInt(String.valueOf(entrada2));
			resultado = n+m;
			
			char[] salida = String.valueOf(resultado).toCharArray();
			
			for(char c : salida) {
				for(char c2 : PARES) {
					if(c==c2) {
						todoImpar = false;
						break;
					}
				}
			}
			
			if(todoImpar) System.out.println("SI");
			else System.out.println("NO");
			
			} catch(Exception e) {}
		} while(true);
		
	}
}
