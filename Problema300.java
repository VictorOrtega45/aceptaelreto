import java.io.BufferedReader;
import java.io.InputStreamReader;
// @author V�ctor Ortega
public class Problema300 {
	public static void main(String[] args) {
		// Declaraci� variables
		int n = 0;	String entrada = "";
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try {n = Integer.parseInt(br.readLine());}
		catch (Exception e) {}
		
		for (int j = n; j>0; j--) {
			
			boolean a = false;
			boolean e = false;
			boolean i = false;
			boolean o = false;
			boolean u = false;
			
			try { entrada = br.readLine();}
			catch (Exception exc) {}
			
			for(char c : entrada.toCharArray()) {
				if(c=='a')a=true;
				if(c=='e')e=true;
				if(c=='i')i=true;
				if(c=='o')o=true;
				if(c=='u')u=true;
			}
			
			if(a&&e&&i&&o&&u) System.out.println("SI");
			else System.out.println("NO");
		}
		
	}
}
