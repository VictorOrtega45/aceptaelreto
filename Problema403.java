import java.util.Scanner;

public class Problema403 
{
	public static void main(String[] args)
	{
		int n;
		double areaTotal, campos;
		
		Scanner sc = new Scanner(System.in);
		
		n = sc.nextInt();
		
		while(n > 0)
		{
			areaTotal = sc.nextInt();
			campos = sc.nextInt();
			
			if(45*90 <= areaTotal/campos && areaTotal/campos <= 120*90)
				System.out.println("SI");
			else System.out.println("NO");
			
			n--;
		}
	}
}
