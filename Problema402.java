import java.util.Scanner;

public class Problema402
{
	public static void main(String[] args)
	{
		int n = 0;
		int raiz;
		
		Scanner sc = new Scanner(System.in);
		
		do 
		{
			n = sc.nextInt();
			if(n == 0) break;
			
			raiz = (int) Math.sqrt(n);
			
			do 
			{
				if(n%raiz==0) 
					if(n/raiz==raiz) break;
					raiz--;
			} while(true);
			
			System.out.println(raiz);
			
		} while(true);
	}
}
