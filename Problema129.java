import java.io.BufferedReader;
import java.io.InputStreamReader;
// import java.util.ArrayList;
// @author V�ctor Ortega

/*
 * 
 * 
 * 
 * 
 */

public class Problema129 {
	
	// Comprovacion
	static int comprovacion(int entrada, int llargada) {
		int vecesTotal = 0;
		int vecesOn = 0;
		switch (entrada) {
			case 0: case 6: case 9: 
				vecesOn = 6; break; 
			case 1: 
				vecesOn = 2; break;
			case 2: case 3: case 5: 
				vecesOn = 5; break;
			case 4: 
				vecesOn = 4; break;			
			case 7:
				vecesOn = 3; break;
			case 8:
				vecesOn = 7;
		}
		
		vecesTotal = (llargada-1)*2*vecesOn;
		return vecesTotal;
	}
	
	// Main
	public static void main(String[] args) {
		// Declaraci� variables
		String entrada = "";	int total = 0;
		int llargada = 0;
		
		// ArrayList<Integer> lista = new ArrayList<Integer>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		do {
			total = 0;
			
			try {entrada = br.readLine();}
			catch (Exception e) {}
			
			llargada = entrada.length()/2;
			int[] lista = new int[llargada];
			
			String[] numeros = entrada.split(" ");
			for(int i = 0; i<llargada; i++) {
				lista[i] = Integer.parseInt(numeros[i]);
				total+= comprovacion(lista[i], llargada);
				
			}
			System.out.println(total);
		} while(!entrada.equals("-1"));
	}	
}