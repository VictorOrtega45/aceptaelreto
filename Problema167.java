import java.util.Scanner;
// @author V�ctor Ortega
public class Problema167 {
	public static void main(String[] args) {
		// Declaraci� variables
		int l = 0;	int multiplicador = 0;	int salida = 0;
		int divisio = 0;	int numCuadrados = 0;
		Scanner sc = new Scanner(System.in);
		
		while(sc.hasNextInt()) {
			l = sc.nextInt();
			multiplicador = 2;
			salida = l*4;
			numCuadrados = 0;
			
			do {
				if(l/multiplicador>=1) {
					divisio = l/multiplicador;
					numCuadrados=(int) Math.pow(multiplicador, 2);
					salida = salida+(divisio*4*numCuadrados);
					multiplicador*=2;
				}
				else break;
				
			} while(true);
			
			
			System.out.println(salida);
		}
	}
}
