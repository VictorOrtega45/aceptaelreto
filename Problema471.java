import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problema471 
{
	public static void main(String[] args) throws Exception
	{
		int n = 0;
		int x = 0;
		int y = 0;
		String entrada;
		final int R = 360;
		final char salt = '\n';
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		n = Integer.parseInt(br.readLine());
		
		while(n>0)
		{
			entrada = br.readLine();
			String[] parts = entrada.split(" ");
			x = Integer.parseInt(parts[0]);
			y = Integer.parseInt(parts[1]);
			
			
			if(Math.abs(x-y) == 0 || Math.abs(x-y) == 180) 
			{
				sb.append("DA IGUAL").append(salt);
				n--;
				continue;
			}
			
			if(x<y)
			{
				if(y-x<180) sb.append("ASCENDENTE");
				else sb.append("DESCENDENTE"); 
			}
			else
			{
				if(x-y>180) sb.append("ASCENDENTE");
				else sb.append("DESCENDENTE"); 
			}
			sb.append(salt);
			
			n--;
		}
		System.out.print(sb);
	}
}
